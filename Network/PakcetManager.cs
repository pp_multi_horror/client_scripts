﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacketManager
{
    public PacketManager()
    {

    }

    public byte[] SetBuffer(UInt64 _protocol, byte[] _buffer, ref int _size)
    {
        byte[] data = new byte[1024];
        int place = 0;

        Buffer.BlockCopy(BitConverter.GetBytes(_protocol), 0, data, place, sizeof(UInt64));
        place += sizeof(UInt64);

        Buffer.BlockCopy(_buffer, 0, data, place, _buffer.Length);
        place += _buffer.Length;

        _size = place;

        return data;
    }

    //GetBytes();
    public byte[] PackPacket(UInt64 _protocol, string _ID, string _PW, ref int _size)
    {
        byte[] data = new byte[1024];
        int place = 0;

        int IDsize = _ID.Length*2;
        int PWsize = _PW.Length*2;

        place += sizeof(int);

        Buffer.BlockCopy(BitConverter.GetBytes(_protocol), 0, data, place, sizeof(UInt64));
        place += sizeof(UInt64);
        _size += sizeof(UInt64);

        Buffer.BlockCopy(BitConverter.GetBytes(IDsize), 0, data, place, sizeof(int));
        place += sizeof(int);
        _size += sizeof(int);

        Buffer.BlockCopy(Encoding.Unicode.GetBytes(_ID), 0, data, place, _ID.Length*2);
        Debug.Log(IDsize);
        place += IDsize;
        _size += IDsize;

        Buffer.BlockCopy(BitConverter.GetBytes(PWsize), 0, data, place, sizeof(int));
        place += sizeof(int);
        _size += sizeof(int);

        Buffer.BlockCopy(Encoding.Unicode.GetBytes(_PW), 0, data, place, _PW.Length*2);
        place += PWsize;
        _size += PWsize;

        place = 0;

        Buffer.BlockCopy(BitConverter.GetBytes(_size), 0, data, place, sizeof(int));

        _size += sizeof(int);

        return data;
    }

    public byte[] PackPacket(UInt64 _protocol, string _string, ref int _size)
    {
        byte[] data = new byte[1024];
        int place = 0;

        int NickNamesize = _string.Length * 2;

        place += sizeof(int);

        Buffer.BlockCopy(BitConverter.GetBytes(_protocol), 0, data, place, sizeof(UInt64));
        place += sizeof(UInt64);
        _size += sizeof(UInt64);

        Buffer.BlockCopy(BitConverter.GetBytes(NickNamesize), 0, data, place, sizeof(int));
        place += sizeof(int);
        _size += sizeof(int);

        Buffer.BlockCopy(Encoding.Unicode.GetBytes(_string), 0, data, place, _string.Length * 2);
        place += NickNamesize;
        _size += NickNamesize;

        place = 0;

        Buffer.BlockCopy(BitConverter.GetBytes(_size), 0, data, place, sizeof(int));

        _size += sizeof(int);

        return data;
    }

    public byte[] PackPacket(string _ID, string _PW)
    {
        byte[] data = new byte[1024];
        int place = 0;

        int IDsize = _ID.Length;
        int PWsize = _PW.Length;

        Buffer.BlockCopy(BitConverter.GetBytes(IDsize), 0, data, place, sizeof(int));
        place += sizeof(int);

        Buffer.BlockCopy(Encoding.ASCII.GetBytes(_ID), 0, data, place, _ID.Length);
        place += IDsize;

        Buffer.BlockCopy(BitConverter.GetBytes(PWsize), 0, data, place, sizeof(int));
        place += sizeof(int);

        Buffer.BlockCopy(Encoding.ASCII.GetBytes(_PW), 0, data, place, _PW.Length);
        place += PWsize;

        return data;
    }

    public byte[] PackPacket(UInt64 _protocol, ref int _size)
    {
        byte[] data = new byte[1024];
        int place = 0;

        place += sizeof(int);

        Buffer.BlockCopy(BitConverter.GetBytes(_protocol), 0, data, place, sizeof(UInt64));
        place += sizeof(UInt64);
        _size += sizeof(UInt64);

        place = 0;

        Buffer.BlockCopy(BitConverter.GetBytes(_size), 0, data, place, sizeof(int));

        _size += sizeof(int);

        return data;
    }

    public byte[] PackPacket(UInt64 _protocol, bool _isReady, ref int _size)
    {
        byte[] data = new byte[1024];
        int place = 0;

        place += sizeof(int);

        Buffer.BlockCopy(BitConverter.GetBytes(_protocol), 0, data, place, sizeof(UInt64));
        place += sizeof(UInt64);
        _size += sizeof(UInt64);

        Buffer.BlockCopy(BitConverter.GetBytes(_isReady), 0, data, place, sizeof(bool));
        place += sizeof(bool);
        _size += sizeof(bool);

        place = 0;

        Buffer.BlockCopy(BitConverter.GetBytes(_size), 0, data, place, sizeof(int));

        _size += sizeof(int);

        return data;
    }

    public byte[] PackPacket(UInt64 _protocol, bool _flag)
    {
        byte[] data = new byte[1024];
        int place = 0;

        place += sizeof(int);

        Buffer.BlockCopy(BitConverter.GetBytes(_protocol), 0, data, place, sizeof(UInt64));
        place += sizeof(UInt64);

        Buffer.BlockCopy(BitConverter.GetBytes(_flag), 0, data, place, sizeof(bool));
        place += sizeof(bool);

        return data;
    }

    public byte[] PackPacket(UInt64 _protocol, Vector3 _pos, Vector2 _aixs, ref int _size)
    {
        byte[] data = new byte[1024];
        int place = 0;

        place += sizeof(int);

        Buffer.BlockCopy(BitConverter.GetBytes(_protocol), 0, data, place, sizeof(UInt64));
        place += sizeof(UInt64);
        _size += sizeof(UInt64);

        Buffer.BlockCopy(BitConverter.GetBytes(_pos.x), 0, data, place, sizeof(float));
        place += sizeof(float);
        _size += sizeof(float);

        Buffer.BlockCopy(BitConverter.GetBytes(_pos.y), 0, data, place, sizeof(float));
        place += sizeof(float);
        _size += sizeof(float);

        Buffer.BlockCopy(BitConverter.GetBytes(_pos.z), 0, data, place, sizeof(float));
        place += sizeof(float);
        _size += sizeof(float);

        Buffer.BlockCopy(BitConverter.GetBytes(_aixs.x), 0, data, place, sizeof(float));
        place += sizeof(float);
        _size += sizeof(float);

        Buffer.BlockCopy(BitConverter.GetBytes(_aixs.y), 0, data, place, sizeof(float));
        place += sizeof(float);
        _size += sizeof(float);

        place = 0;

        Buffer.BlockCopy(BitConverter.GetBytes(_size), 0, data, place, sizeof(int));

        _size += sizeof(int);

        return data;
    }

    public byte[] PackPacket(UInt64 _protocol, Vector3 _pos, Vector2 _aixs, float _pitch, float _yaw, ref int _size)
    {
        byte[] data = new byte[1024];
        int place = 0;

        place += sizeof(int);

        Buffer.BlockCopy(BitConverter.GetBytes(_protocol), 0, data, place, sizeof(UInt64));
        place += sizeof(UInt64);
        _size += sizeof(UInt64);

        Buffer.BlockCopy(BitConverter.GetBytes(_pos.x), 0, data, place, sizeof(float));
        place += sizeof(float);
        _size += sizeof(float);

        Buffer.BlockCopy(BitConverter.GetBytes(_pos.y), 0, data, place, sizeof(float));
        place += sizeof(float);
        _size += sizeof(float);

        Buffer.BlockCopy(BitConverter.GetBytes(_pos.z), 0, data, place, sizeof(float));
        place += sizeof(float);
        _size += sizeof(float);

        Buffer.BlockCopy(BitConverter.GetBytes(_aixs.x), 0, data, place, sizeof(float));
        place += sizeof(float);
        _size += sizeof(float);

        Buffer.BlockCopy(BitConverter.GetBytes(_aixs.y), 0, data, place, sizeof(float));
        place += sizeof(float);
        _size += sizeof(float);

        Buffer.BlockCopy(BitConverter.GetBytes(_pitch), 0, data, place, sizeof(float));
        place += sizeof(float);
        _size += sizeof(float);

        Buffer.BlockCopy(BitConverter.GetBytes(_yaw), 0, data, place, sizeof(float));
        place += sizeof(float);
        _size += sizeof(float);

        place = 0;

        Buffer.BlockCopy(BitConverter.GetBytes(_size), 0, data, place, sizeof(int));

        _size += sizeof(int);

        return data;
    }

    public byte[] PackPacket(UInt64 _protocol, float _pitch, float _yaw, ref int _size)
    {
        byte[] data = new byte[1024];
        int place = 0;

        place += sizeof(int);

        Buffer.BlockCopy(BitConverter.GetBytes(_protocol), 0, data, place, sizeof(UInt64));
        place += sizeof(UInt64);
        _size += sizeof(UInt64);

        Buffer.BlockCopy(BitConverter.GetBytes(_pitch), 0, data, place, sizeof(float));
        place += sizeof(float);
        _size += sizeof(float);

        Buffer.BlockCopy(BitConverter.GetBytes(_yaw), 0, data, place, sizeof(float));
        place += sizeof(float);
        _size += sizeof(float);

        place = 0;

        Buffer.BlockCopy(BitConverter.GetBytes(_size), 0, data, place, sizeof(int));

        _size += sizeof(int);

        return data;
    }

    public byte[] PackPacket(UInt64 _protocol, string m_Lobby_Name, int m_ID, ref int _size)
    {
        byte[] data = new byte[1024];
        int place = 0;

        int m_Lobby_NameSize = m_Lobby_Name.Length * 2;

        place += sizeof(int);

        Buffer.BlockCopy(BitConverter.GetBytes(_protocol), 0, data, place, sizeof(UInt64));
        place += sizeof(UInt64);
        _size += sizeof(UInt64);
        Debug.Log("protocol = " + _protocol);

        Buffer.BlockCopy(BitConverter.GetBytes(m_Lobby_NameSize), 0, data, place, sizeof(int));
        place += sizeof(int);
        _size += sizeof(int);
        Debug.Log("strSize = " + m_Lobby_NameSize);

        Buffer.BlockCopy(Encoding.Unicode.GetBytes(m_Lobby_Name), 0, data, place, m_Lobby_NameSize);
        place += m_Lobby_NameSize;
        _size += m_Lobby_NameSize;
        Debug.Log("str = " + m_Lobby_Name);

        Buffer.BlockCopy(BitConverter.GetBytes(m_ID), 0, data, place, sizeof(int));
        place += sizeof(int);
        _size += sizeof(int);

        Debug.Log("roomNum = " + m_ID);

        place = 0;

        Buffer.BlockCopy(BitConverter.GetBytes(_size), 0, data, place, sizeof(int));

        _size += sizeof(int);

        return data;
    }

    // 2019_08_26(CHOI_작업)
    public byte[] PackPacket(UInt64 _protocol, int _NetID, int _objectID, int _activate, bool _instantly, ref int _size)
    {
        byte[] data = new byte[1024];
        int place = 0;

        place += sizeof(int);

        Buffer.BlockCopy(BitConverter.GetBytes(_protocol), 0, data, place, sizeof(UInt64));
        place += sizeof(UInt64);
        _size += sizeof(UInt64);
        // 넷아이디
        Buffer.BlockCopy(BitConverter.GetBytes(_NetID), 0, data, place, sizeof(int));
        place += sizeof(int);
        _size += sizeof(int);
        // 오브젝트 번호
        Buffer.BlockCopy(BitConverter.GetBytes(_objectID), 0, data, place, sizeof(int));
        place += sizeof(int);
        _size += sizeof(int);
        // 어떻게 하였는가
        Buffer.BlockCopy(BitConverter.GetBytes(_activate), 0, data, place, sizeof(int));
        place += sizeof(int);
        _size += sizeof(int);
        // 애니메이션 과정
        Buffer.BlockCopy(BitConverter.GetBytes(_instantly), 0, data, place, sizeof(bool));
        place += sizeof(bool);
        _size += sizeof(bool);

        place = 0;

        Buffer.BlockCopy(BitConverter.GetBytes(_size), 0, data, place, sizeof(int));

        _size += sizeof(int);

        return data;
    }

    public void GetProtocol(byte[] _data, ref UInt64 _protocol)
    {
        int place = 0;

        _protocol = BitConverter.ToUInt64(_data, place);
        place += sizeof(UInt64);
    }

    public void PackProtocol(ref UInt64 _protocol, UInt64 __protocol)
    {
        _protocol = _protocol | __protocol;
    }

    public void PacketRecv(SocketConnect _sock, ref byte[] _buf) // 총 버퍼 받아오기
    {
       
        byte[] size = new byte[4];

        NetworkStream ns = _sock.GetClient().GetStream();

        int recv = ns.Read(size, 0, 4);

        int packsize = BitConverter.ToInt16(size, 0);

        recv = ns.Read(_buf, 0, packsize);
    }

    public void UnPackPacket(byte[] _data, ref UInt64 _protocol, ref int _m_ID)
    {
        int place = 0;

        _protocol = BitConverter.ToUInt64(_data, place);
        place += sizeof(UInt64);

        _m_ID = BitConverter.ToInt32(_data, place);
        place += sizeof(int);
    }
}