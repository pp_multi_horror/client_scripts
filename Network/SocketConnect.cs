﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocketConnect
{
    private TcpClient client;

    //private void Awake()
    //{
    //    client = new TcpClient();
    //}

    public SocketConnect()
    {
        //client = new TcpClient();
    }

    public TcpClient GetClient()
    {
        return client;
    }

    public void Connect(string _ip, string _port)
    {
        client = new TcpClient();
        client.Connect(_ip, int.Parse(_port));
    }

    public void Disconnect()
    {
        client.Close();
    }
}

