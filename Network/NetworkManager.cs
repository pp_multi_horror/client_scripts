﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LoginManager_Protocol : UInt64
{
    CONNECT = 0x0001000000000000
};

public enum LobbyListManager_Protocol : UInt64
{
    CREATE  = 0x0001000000000000,
    ENTER   = 0x0002000000000000,
    SHOW    = 0x0004000000000000,
    LOGOUT  = 0x0008000000000000
};

public enum LobbyManager_Protocol : UInt64
{
    LOBBY_INIT          = 0x0001000000000000,
    PLAYERLIST          = 0x0002000000000000, // + READY
    READY               = 0x0004000000000000,
    CHATTING            = 0x0008000000000000,
    EXIT                = 0x0010000000000000,
    LOBBY_GAME_START    = 0x0020000000000000
}

public enum InGameManagerProtocol : UInt64
{
    GAME_INIT           = 0x0001000000000000,
    PING                = 0x0002000000000000,
    LOADING_READY       = 0x0004000000000000,
    GAME_PLAYER_DATA    = 0x0008000000000000,
    GAME_START          = 0x0010000000000000,

    #region PROTOCOL_Move
    MOVE                = 0x0020000000000000,

    POSITION            = 0x0000000100000000,
    ROTATION            = 0x0000000200000000,
    LOOK                = 0x0000000400000000,
    CROUCH              = 0x0000000010000000,
    DASH                = 0x0000000020000000,
    #endregion

    LEAVE               = 0x0040000000000000,
    END                 = 0x0080000000000000,
    ACTIVE_OBJ          = 0x0100000000000000,
    SECTOR_LIST         = 0x0200000000000000,
    UPDATE_USER_LIST    = 0x0400000000000000,
    OBJECT              = 0x0800000000000000 // 2019_08_26(CHOI_작업)
};

[RequireComponent(typeof(LoginManager))]

public class NetworkManager : MonoBehaviour
{
    LoginManager login;
    public PacketManager packet;
    SocketConnect socket;

    public static NetworkManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            packet = new PacketManager();
            socket = new SocketConnect();
        }

        if (Instance != null)
        {
            Destroy(gameObject);
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void SendProtocol(UInt64 _protocol)
    {
        int dataSize = 0;

        byte[] data = packet.PackPacket(_protocol, ref dataSize);

        NetworkStream ns;

        ns = socket.GetClient().GetStream();

        ns.Write(data, 0, dataSize);
    }

    public void PacketRecv(ref byte[] _buf) // 총 버퍼 받아오기
    {

        byte[] size = new byte[4];

        NetworkStream ns = socket.GetClient().GetStream();

        int recv = ns.Read(size, 0, 4);

        int packsize = BitConverter.ToInt16(size, 0);

        recv = ns.Read(_buf, 0, packsize);
    }

    public void Login(string _nickname)
    {
        int dataSize = 0;

        try
        {
            //프로토콜, 버퍼, 버퍼사이즈
            //byte[] buffer = packet.PackPacket(login.ID, login.PW);

            //byte[] data = packet.SetBuffer((UInt64)LoginServer_Protocol.LOGIN_INFO, buffer, ref dataSize);

            byte[] data = packet.PackPacket((UInt64)LoginManager_Protocol.CONNECT, _nickname, ref dataSize);

            NetworkStream ns;

            ns = socket.GetClient().GetStream();

            ns.Write(data, 0, dataSize);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }

        //}
    }

    public void Join(string _ip, string _port)
    {
        int dataSize = 0;

        Debug.Log("로그인 시도!!!");

        try
        {
            //프로토콜, 버퍼, 버퍼사이즈
            //byte[] buffer = packet.PackPacket(login.ID, login.PW);

            //byte[] data = packet.SetBuffer((UInt64)LoginServer_Protocol.LOGIN_INFO, buffer, ref dataSize);

            byte[] data = packet.PackPacket((UInt64)LoginManager_Protocol.CONNECT, _ip, _port, ref dataSize);

            NetworkStream ns;

            ns = socket.GetClient().GetStream();

            ns.Write(data, 0, dataSize);
            Debug.Log("보내짔나");
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }

        //}
    }

    public void SeverConnet(string _IP,string _Port) //서버와 연결하는 함수
    {
        socket.Connect(_IP, _Port);
    }

    public void PackProtocol(ref UInt64 _protocol, UInt64 __protocol)
    {
        _protocol = _protocol | __protocol;
    }

    public bool LoginResult()
    {
        byte[] data = new byte[1024];
        
        packet.PacketRecv(socket,ref data); // 데이터 받아오기
        
        UInt64 protocol;
        int str1;
        int result;
        string msg;

        int place = 0;

        protocol = BitConverter.ToUInt64(data, place);
        place += sizeof(UInt64);

        Debug.Log("protocol = " + protocol);

        result = BitConverter.ToInt32(data, place);
        place += sizeof(int);

        Debug.Log("result = " + result);

        str1 = BitConverter.ToInt32(data, place);
        place += sizeof(int);

        Debug.Log("str1 = " + str1);

        msg = Encoding.Unicode.GetString(data, place, str1);
        place += str1;

        Debug.Log("msg = " + msg);

        if(result == 5)
        {
            return true;
        }
        return false;
    }

    #region LobbyListState

    public LobbyListManager_Protocol LobbyListState_UnPackProtocol(ref UInt64 _protocol)
    {
        if ((_protocol & (UInt64)LobbyListManager_Protocol.CREATE) == (UInt64)LobbyListManager_Protocol.CREATE)
        {
            return LobbyListManager_Protocol.CREATE;
        }
        else if ((_protocol & (UInt64)LobbyListManager_Protocol.ENTER) == (UInt64)LobbyListManager_Protocol.ENTER)
        {
            return LobbyListManager_Protocol.ENTER;
        }
        else if ((_protocol & (UInt64)LobbyListManager_Protocol.SHOW) == (UInt64)LobbyListManager_Protocol.SHOW)
        {
            return LobbyListManager_Protocol.SHOW;
        }
        else if ((_protocol & (UInt64)LobbyListManager_Protocol.LOGOUT) == (UInt64)LobbyListManager_Protocol.LOGOUT)
        {
            return LobbyListManager_Protocol.LOGOUT;
        }

        return 0;
    }

    public int CreateLobby() // 방만들자
    {
        int dataSize = 0;

        byte[] data = packet.PackPacket((UInt64)LobbyListManager_Protocol.CREATE, ref dataSize);

        NetworkStream ns;

        ns = socket.GetClient().GetStream();

        ns.Write(data, 0, dataSize);

        byte[] data2 = new byte[1024];

        packet.PacketRecv(socket, ref data2);

        UInt64 protocol = 0;
        int m_ID = 0;

        packet.UnPackPacket(data2, ref protocol, ref m_ID);

        return m_ID;
    }

    public void ShowLobby() // 서버에게 로비를 보여줄것을 요청하는 함수
    {
        int dataSize = 0;

        byte[] data = packet.PackPacket((UInt64)LobbyListManager_Protocol.SHOW, ref dataSize);

        NetworkStream ns;

        ns = socket.GetClient().GetStream();

        ns.Write(data, 0, dataSize);
    }

    public int LobbyCount() // 요청받은 로비가 몇개인지 판단할 함수
    {
        byte[] data = new byte[1024];

        packet.PacketRecv(socket, ref data); // 데이터 받아오기

        UInt64 protocol;
        int roomCount = 0;

        int place = 0;

        protocol = BitConverter.ToUInt64(data, place);
        place += sizeof(UInt64);

        roomCount = BitConverter.ToInt32(data, place);
        place += sizeof(int);

        return roomCount;
    }

    public void LobbyInfo(ref int roomNumber, ref string id, ref int playerCount, ref bool isPlay) // 로비 정보들을 반환할 함수
    {
        byte[] data = new byte[1024];

        packet.PacketRecv(socket, ref data); // 데이터 받아오기

        int idSize;

        int place = 0;
        place += sizeof(UInt64);

        idSize = BitConverter.ToInt32(data, place);
        place += sizeof(int);

        //Debug.Log("id size = " + idSize);

        id = Encoding.Unicode.GetString(data, place, idSize);
        place += idSize;

        //Debug.Log("id = " + id);

        roomNumber = BitConverter.ToInt32(data, place);
        place += sizeof(int);

        //Debug.Log("roomnumber = " + roomNumber);

        playerCount = BitConverter.ToInt32(data, place);
        place += sizeof(int);

        //Debug.Log("playercount = " + playerCount);

        isPlay = BitConverter.ToBoolean(data, place);
        place += sizeof(bool);

        //Debug.Log("play?? = " + isPlay);


    }

    public void LogOut()
    {
        socket.Disconnect();
    }

    public bool EnterLobbyResult()
    {
        byte[] data = new byte[1024];

        packet.PacketRecv(socket, ref data); // 데이터 받아오기

        bool result;
        int place = 0;

        place += sizeof(UInt64);

        result = BitConverter.ToBoolean(data, place);
        place += sizeof(bool);

        return result;
    }

    public void EnterLobby(int _m_ID, string _m_Lobby_Name)
    {
        int dataSize = 0;

        byte[] data = packet.PackPacket((UInt64)LobbyListManager_Protocol.ENTER, _m_Lobby_Name, _m_ID, ref dataSize);

        NetworkStream ns;

        ns = socket.GetClient().GetStream();

        ns.Write(data, 0, dataSize);
    }

    #endregion

    #region LobbyState

    public LobbyManager_Protocol LobbyState_UnPackProtocol(ref UInt64 _protocol)
    {
        if ((_protocol & (UInt64)LobbyManager_Protocol.LOBBY_INIT) == (UInt64)LobbyManager_Protocol.LOBBY_INIT)
        {
            return LobbyManager_Protocol.LOBBY_INIT;
        }
        else if ((_protocol & (UInt64)LobbyManager_Protocol.PLAYERLIST) == (UInt64)LobbyManager_Protocol.PLAYERLIST)
        {
            return LobbyManager_Protocol.PLAYERLIST;
        }
        else if ((_protocol & (UInt64)LobbyManager_Protocol.READY) == (UInt64)LobbyManager_Protocol.READY)
        {
            return LobbyManager_Protocol.READY;
        }
        else if ((_protocol & (UInt64)LobbyManager_Protocol.CHATTING) == (UInt64)LobbyManager_Protocol.CHATTING)
        {
            return LobbyManager_Protocol.CHATTING;
        }
        else if ((_protocol & (UInt64)LobbyManager_Protocol.EXIT) == (UInt64)LobbyManager_Protocol.EXIT)
        {
            return LobbyManager_Protocol.EXIT;
        }
        else if ((_protocol & (UInt64)LobbyManager_Protocol.LOBBY_GAME_START) == (UInt64)LobbyManager_Protocol.LOBBY_GAME_START)
        {
            return LobbyManager_Protocol.LOBBY_GAME_START;
        }

        return 0;
    }

    public void ExitLobby()
    {
        int dataSize = 0;

        byte[] data = packet.PackPacket((UInt64)LobbyManager_Protocol.EXIT, ref dataSize);

        NetworkStream ns;

        ns = socket.GetClient().GetStream();

        ns.Write(data, 0, dataSize);
    }

    public void S_LobbyInit()
    {
        int dataSize = 0;

        byte[] data = packet.PackPacket((UInt64)LobbyManager_Protocol.LOBBY_INIT, ref dataSize);

        NetworkStream ns;

        ns = socket.GetClient().GetStream();

        ns.Write(data, 0, dataSize);
    }

    public void R_LobbyInit(byte[] _data, ref int _NetID)
    {
        int place = 0;

        place += sizeof(UInt64);

        _NetID = BitConverter.ToInt32(_data, place);
        place += sizeof(int);
    }

    public void SetPlayerList(byte[] _data, ref int _NetID, ref string _NickName, ref bool _isReady)
    {
        int place = 0;
        int NickSize;

        place += sizeof(UInt64);

        _NetID = BitConverter.ToInt32(_data, place);
        place += sizeof(int);

        NickSize = BitConverter.ToInt32(_data, place);
        place += sizeof(int);

        _NickName = Encoding.Unicode.GetString(_data, place, NickSize);
        place += NickSize;

        _isReady = BitConverter.ToBoolean(_data, place);
        place += sizeof(bool);
        
    }

    public void ExitPlayer(byte[] _data, ref int _m_ID)
    {
        int place = 0;

        place += sizeof(UInt64);

        _m_ID = BitConverter.ToInt32(_data, place);
        place += sizeof(int);
    }

    public void S_Ready(bool _isReady)
    {
        int dataSize = 0;

        byte[] data = packet.PackPacket((UInt64)LobbyManager_Protocol.READY, _isReady, ref dataSize);

        NetworkStream ns;

        ns = socket.GetClient().GetStream();

        ns.Write(data, 0, dataSize);
    }

    public void R_Ready(byte[] _data, ref int _NetID, ref bool _isReady)
    {
        int place = 0;

        place += sizeof(UInt64);

        _NetID = BitConverter.ToInt32(_data, place);
        place += sizeof(int);

        _isReady = BitConverter.ToBoolean(_data, place);
        place += sizeof(bool);
    }

    public void S_Chat(string _msg)
    {
        int dataSize = 0;
        Debug.Log((UInt64)LobbyManager_Protocol.CHATTING);

        byte[] data = packet.PackPacket((UInt64)LobbyManager_Protocol.CHATTING, _msg, ref dataSize);

        NetworkStream ns;

        ns = socket.GetClient().GetStream();

        ns.Write(data, 0, dataSize);
    }

    public void R_Chat(byte[] _data, ref string _msg)
    {
        int msgSize = 0;

        int place = 0;

        place += sizeof(UInt64);

        msgSize = BitConverter.ToInt32(_data, place);
        place += sizeof(int);

        _msg = Encoding.Unicode.GetString(_data, place, msgSize);
        place += msgSize;
    }
    #endregion

    #region GameState

    public InGameManagerProtocol GameState_UnPackProtocol(UInt64 _protocol)
    {
        if ((_protocol & (UInt64)InGameManagerProtocol.GAME_INIT) == (UInt64)InGameManagerProtocol.GAME_INIT)
        {
            return InGameManagerProtocol.GAME_INIT;
        }
        else if ((_protocol & (UInt64)InGameManagerProtocol.PING) == (UInt64)InGameManagerProtocol.PING)
        {
            return InGameManagerProtocol.PING;
        }
        else if ((_protocol & (UInt64)InGameManagerProtocol.LOADING_READY) == (UInt64)InGameManagerProtocol.LOADING_READY)
        {
            return InGameManagerProtocol.LOADING_READY;
        }
        else if ((_protocol & (UInt64)InGameManagerProtocol.GAME_PLAYER_DATA) == (UInt64)InGameManagerProtocol.GAME_PLAYER_DATA)
        {
            return InGameManagerProtocol.GAME_PLAYER_DATA;
        }
        else if ((_protocol & (UInt64)InGameManagerProtocol.GAME_START) == (UInt64)InGameManagerProtocol.GAME_START)
        {
            return InGameManagerProtocol.GAME_START;
        }
        else if ((_protocol & (UInt64)InGameManagerProtocol.MOVE) == (UInt64)InGameManagerProtocol.MOVE)
        {
            return InGameManagerProtocol.MOVE;
        }
        else if ((_protocol & (UInt64)InGameManagerProtocol.LEAVE) == (UInt64)InGameManagerProtocol.LEAVE)
        {
            return InGameManagerProtocol.LEAVE;
        }
        else if ((_protocol & (UInt64)InGameManagerProtocol.END) == (UInt64)InGameManagerProtocol.END)
        {
            return InGameManagerProtocol.END;
        }
        else if ((_protocol & (UInt64)InGameManagerProtocol.ACTIVE_OBJ) == (UInt64)InGameManagerProtocol.ACTIVE_OBJ)
        {
            return InGameManagerProtocol.ACTIVE_OBJ;
        }
        else if ((_protocol & (UInt64)InGameManagerProtocol.SECTOR_LIST) == (UInt64)InGameManagerProtocol.SECTOR_LIST)
        {
            // 2019_08_26(CHOI_작업)
            return InGameManagerProtocol.SECTOR_LIST;
        }
        else if ((_protocol & (UInt64)InGameManagerProtocol.UPDATE_USER_LIST) == (UInt64)InGameManagerProtocol.UPDATE_USER_LIST)
        {
            // 2019_08_26(CHOI_작업)
            return InGameManagerProtocol.UPDATE_USER_LIST;
        }
        else if ((_protocol & (UInt64)InGameManagerProtocol.OBJECT) == (UInt64)InGameManagerProtocol.OBJECT)
        {
            // 2019_08_26(CHOI_작업)
            return InGameManagerProtocol.OBJECT;
        }

        return 0;
    }

    public void S_Ping()
    {
        int dataSize = 0;

        byte[] data = packet.PackPacket((UInt64)InGameManagerProtocol.PING, ref dataSize);

        NetworkStream ns;

        ns = socket.GetClient().GetStream();

        ns.Write(data, 0, dataSize);
    }

    public void Loading_Ready()
    {
        int dataSize = 0;

        byte[] data = packet.PackPacket((UInt64)InGameManagerProtocol.LOADING_READY, ref dataSize);

        NetworkStream ns;

        ns = socket.GetClient().GetStream();

        ns.Write(data, 0, dataSize);
    }

    public void S_Position(UInt64 _protocol, Vector3 _position, Vector2 _axis)
    {
        int dataSize = 0;

        byte[] data = packet.PackPacket(_protocol, _position, _axis, ref dataSize);

        NetworkStream ns;

        ns = socket.GetClient().GetStream();

        ns.Write(data, 0, dataSize);
    }

    public void R_Position(byte[] _data, ref int _NetID, ref Vector3 _Pos, ref Vector2 _axis, ref int _dataSize)
    {
        int place = 0;

        place += sizeof(UInt64);

        _NetID = BitConverter.ToInt32(_data, place);
        place += sizeof(int);

        _Pos.x = BitConverter.ToSingle(_data, place);
        place += sizeof(float);

        _Pos.y = BitConverter.ToSingle(_data, place);
        place += sizeof(float);

        _Pos.z = BitConverter.ToSingle(_data, place);
        place += sizeof(float);

        _axis.x = BitConverter.ToSingle(_data, place);
        place += sizeof(float);

        _axis.y = BitConverter.ToSingle(_data, place);
        place += sizeof(float);

        _dataSize = place;
    }

    public void R_Player_Data(byte[] _data, ref bool _isPhantom, ref int _NetID, ref Vector3 _Pos)
    {
        int place = 0;

        place += sizeof(UInt64);

        _isPhantom = BitConverter.ToBoolean(_data, place);
        place += sizeof(bool);

        _NetID = BitConverter.ToInt32(_data, place);
        place += sizeof(int);

        _Pos.x = BitConverter.ToSingle(_data, place);
        place += sizeof(float);
        Debug.Log("PosX = " + _Pos.x);

        _Pos.y = BitConverter.ToSingle(_data, place);
        place += sizeof(float);
        Debug.Log("PosY = " + _Pos.y);

        _Pos.z = BitConverter.ToSingle(_data, place);
        place += sizeof(float);
        Debug.Log("PosZ = " + _Pos.z);
    }

    public void S_Move(UInt64 _protocol, Vector3 _position, Vector2 _axis, Vector2 _look)
    {
        int dataSize = 0;

        byte[] data = packet.PackPacket(_protocol, _position, _axis, _look.x, _look.y, ref dataSize);

        NetworkStream ns;

        ns = socket.GetClient().GetStream();

        ns.Write(data, 0, dataSize);
    }

    public void S_Rotation(UInt64 _protocol, Vector2 _look)
    {
        int dataSize = 0;

        byte[] data = packet.PackPacket(_protocol, _look.x, _look.y, ref dataSize);

        NetworkStream ns;

        ns = socket.GetClient().GetStream();

        ns.Write(data, 0, dataSize);
    }

    public void R_Rotation(byte[] _data, ref int _NetID, ref float _pitch, ref float _yaw)
    {
        int place = 0;

        place += sizeof(UInt64);
        
        _NetID = BitConverter.ToInt32(_data, place);
        place += sizeof(int);

        _pitch = BitConverter.ToSingle(_data, place);
        place += sizeof(float);

        _yaw = BitConverter.ToSingle(_data, place);
        place += sizeof(float);
    }

    public void R_Rotation(byte[] _data, ref int _NetID, ref float _pitch, ref float _yaw, int dataSize)
    {
        int place = 0;

        place += sizeof(UInt64);
        place += dataSize;

        _NetID = BitConverter.ToInt32(_data, place);
        place += sizeof(int);

        _pitch = BitConverter.ToSingle(_data, place);
        place += sizeof(float);

        _yaw = BitConverter.ToSingle(_data, place);
        place += sizeof(float);
    }

    public void DisableObj(byte[] _data, ref int _NetID)
    {
        int place = 0;

        place += sizeof(UInt64);

        _NetID = BitConverter.ToInt32(_data, place);
        place += sizeof(int);
    }

    public void ActiveObj(byte[] _data, ref int[] _NetID)
    {
        int place = 0;
        int userCount = 0;

        place += sizeof(UInt64);

        userCount = BitConverter.ToInt32(_data, place);
        place += sizeof(int);

        Debug.Log("유저 수 = " + userCount);

        for (int i = 0; i < userCount; i++)
        {
            _NetID[i] = BitConverter.ToInt32(_data, place);
            place += sizeof(int);
            Debug.Log("번호 = " + _NetID[i]);
        }
    }

    public void S_BoolData(UInt64 _protocol, bool _flag)
    {
        int dataSize = 0;

        byte[] data = packet.PackPacket(_protocol, _flag, ref dataSize);

        NetworkStream ns;

        ns = socket.GetClient().GetStream();

        ns.Write(data, 0, dataSize);

    }

    public void R_Dash(byte[] _data, ref int _NetID, ref bool _flag)
    {
        int place = 0;

        place += sizeof(UInt64);

        _NetID = BitConverter.ToInt32(_data, place);
        place += sizeof(int);

        _flag = BitConverter.ToBoolean(_data, place);
        place += sizeof(bool);
    }

    public void R_Crouch(byte[] _data, ref int _NetID, ref bool _flag)
    {
        int place = 0;

        place += sizeof(UInt64);

        _NetID = BitConverter.ToInt32(_data, place);
        place += sizeof(int);

        _flag = BitConverter.ToBoolean(_data, place);
        place += sizeof(bool);
    }

    // 2019_08_26(CHOI_작업)
    public void S_Object(UInt64 _protocol, int _playerNetID, int _objectID, int _activate, bool _instantly)
    {
        int dataSize = 0;

        byte[] data = packet.PackPacket(_protocol, _playerNetID, _objectID, _activate, _instantly, ref dataSize);

        NetworkStream ns;

        ns = socket.GetClient().GetStream();

        ns.Write(data, 0, dataSize);
    }

    // 2019_08_26(CHOI_작업)
    public void R_Object(byte[] _data, ref int _playerNetID, ref int _objectID, ref int _activate, ref bool _instantly)
    {
        int place = 0;

        place += sizeof(UInt64);

        _playerNetID = BitConverter.ToInt32(_data, place);
        place += sizeof(int);

        _objectID = BitConverter.ToInt32(_data, place);
        place += sizeof(int);

        _activate = BitConverter.ToInt32(_data, place);
        place += sizeof(int);

        _instantly = BitConverter.ToBoolean(_data, place);
        place += sizeof(bool);

    }

    #endregion
}
