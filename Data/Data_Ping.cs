﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Malaia.Data
{
    /// <summary>
    /// 핑 데이터
    /// </summary>
    public class Data_Ping
    {
        public int m_Average; // 핑 평균값
        public int m_Last; // 핑 마지막 값
        public int m_Count; // 핑 기록 횟수
        public int m_Ping_Start_Time; // 핑 기록을 시작하는 시간
        public List<int> m_PingList; // 받은 핑 값

        bool Ping_Test_End;

        public Data_Ping()
        {
            m_Average = 0;
            m_Last = 0;
            m_PingList = new List<int>();
            m_Ping_Start_Time = (int)(Time.realtimeSinceStartup * 1000);
            Ping_Test_End = false;
        }

        public bool Update_Ping()
        {
            m_Count++;
            if (m_Count > 999) m_Count = 0;

            bool Loading_Complete = false;

            int catched_time = (int)(Time.realtimeSinceStartup * 1000);
            m_PingList.Insert(0, (catched_time - m_Ping_Start_Time) / 2);
            m_Last = (catched_time - m_Ping_Start_Time) / 2;

            if (m_PingList.Count > 3)
            {
                if (!Ping_Test_End)
                {
                    Ping_Test_End = true;
                }
                m_PingList.RemoveAt(3);

                Loading_Complete = true;
            }

            // 핑의 평균값 구하기
            int average = 0;
            for (int i = 0; i < m_PingList.Count; i++)
                average += m_PingList[i];
            m_Average = average / m_PingList.Count;

            m_Ping_Start_Time = catched_time;

            //Debug.Log("핑값 = " + Loading_Complete);

            return Loading_Complete;
        }

        public void Start_Check()
        {
            m_Ping_Start_Time = (int)(Time.realtimeSinceStartup * 1000);
        }

        public string Get_PingData()
        {
            return m_Last.ToString("D3") + " / " + m_Average.ToString("D3") + " / " + m_Count.ToString("D3");
        }
    }
}
