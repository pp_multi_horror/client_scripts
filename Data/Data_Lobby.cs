﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Malaia.Data
{

    /// <summary>
    /// 로비 데이터
    /// </summary>
    public class Data_Lobby
    {
        /// <summary> 현재 접속한 로비 </summary>
        public static Data_Lobby Current_Lobby;

        #region SERVER SIDE VAR

        public int m_ID; // 로비의 네트워크 ID
        public string m_Lobby_Name; // 로비 이름
        public int m_Current_User; // 현재 유저 수
        public bool m_Now_Playing; // 현재 플레이중?
        public int m_LobbyLeaderID; // 로비의 방장 ID

        #endregion

        #region CLIENT SIDE VAR

        public List<Data_Player> m_Players; // 안에 있는 플레이어들

        #endregion

        public Data_Lobby()
        {
            m_Players = new List<Data_Player>();
        }

        /// <summary>
        /// 리더의 이름을 얻어오기
        /// </summary>
        public void Server_Get_LeaderName()
        {
            // TODO 서버에게 m_LobbyLeaderID의 ID를 가진 플레이어의 이름 달라고 하기
        }

        /// <summary>
        /// 리더의 이름을 얻었을 때 방의 이름을 수정하기
        /// </summary>
        /// <param name="_name"></param>
        public void Set_RoomName(string _username)
        {
            m_Lobby_Name = _username + "\'s Room";
        }
    }
}
