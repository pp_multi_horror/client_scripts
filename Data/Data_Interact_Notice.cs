﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * 상호작용 알림 (크로스헤어 밑에 뜨는거)
 */
namespace Malaia.Data
{
    public class Data_Interact_Notice
    {
        public static string GetDesc(INTERACT_DESCTYPE type)
        {
            switch(type)
            {
                case INTERACT_DESCTYPE.DOOR:
                    return "문 여닫기";
                case INTERACT_DESCTYPE.WINDOW:
                    return "창문 여닫기";
                case INTERACT_DESCTYPE.LAMP:
                    return "형광등 작동";
            }
            return "건드리기";
        }

        public static string GetDesc_Item(ITEM_INDEX _index)
        {
            switch (_index)
            {
                case ITEM_INDEX.FLASHLIGHT:
                    return "손전등";
                case ITEM_INDEX.CAM:
                    return "캠코더";
                case ITEM_INDEX.BREAD:
                    return "빵";
                case ITEM_INDEX.MILK:
                    return "우유";
            }
            return "???";
        }
    }
}
