﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Malaia.Packet
{
    /// <summary>
    /// 근본 패킷
    /// 이대로는 사용할 수 없다
    /// </summary>
    public abstract class Packet_Base
    {
        public abstract void Send(NetworkManager _network_manager, ref ulong _protocol);
        public abstract void Recv(Ingame_Packet_Handler _packet_handler);
    }

    #region SYSTEM

    // TODO 핑
    public class SystemPingPacket : Packet_Base
    {
        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
            _network_manager.S_Ping();
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            Ingame_Packet_Handler.Instance.e_Ping.Invoke();
        }
    }

    // TODO 게임 시작
    public class SystemGameStartPacket : Packet_Base
    {
        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            Ingame_Packet_Handler.Instance.e_GameStart.Invoke();
        }
    }

    #endregion

    #region MAP

    // TODO 플레이어 소환
    public class PlayerSpawnPacket : Packet_Base
    {
        bool m_Is_Phantom;
        int m_NetID;
        Vector3 m_Coord;

        public PlayerSpawnPacket(bool _is_phantom, int _netID, Vector3 _coord)
        {
            m_Is_Phantom = _is_phantom;
            m_NetID = _netID;
            m_Coord = _coord;
        }

        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            _packet_handler.m_PlayerSpawn.Invoke(m_Is_Phantom, m_NetID, m_Coord);
        }
    }

    // TODO 고스트 소환
    public class GhostSpawnPacket : Packet_Base
    {
        int m_GhostID;
        Vector3 m_Coord;

        public GhostSpawnPacket(int _ghostID, Vector3 _coord)
        {
            m_GhostID = _ghostID;
            m_Coord = _coord;
        }

        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            _packet_handler.m_GhostSpawn.Invoke(m_GhostID, m_Coord);
        }
    }

    // TODO 고스트 소멸
    public class GhostDeSpawnPacket : Packet_Base
    {
        int m_GhostID;

        public GhostDeSpawnPacket(int _ghostID)
        {
            m_GhostID = _ghostID;
        }

        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            _packet_handler.m_GhostDeSpawn.Invoke(m_GhostID);
        }
    }

    // TODO 아이템 소환
    public class ItemSpawnPacket : Packet_Base
    {
        Vector3 m_Coord;
        Vector3 m_EulerAngle;
        int m_ItemID;
        int m_ItemType;

        public ItemSpawnPacket(Vector3 _coord, Vector3 _eulerangle, int _itemID, int _itemtype)
        {
            m_Coord = _coord;
            m_EulerAngle = _eulerangle;
            m_ItemID = _itemID;
            m_ItemType = _itemtype;
        }

        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            _packet_handler.m_ItemSpawn.Invoke(m_Coord, m_EulerAngle, m_ItemID, m_ItemType);
        }
    }

    // TODO 섹터 변경
    public class SectorChangePacket : Packet_Base
    {
        int[] m_NetIDs;

        public SectorChangePacket(int[] _netIDs)
        {
            m_NetIDs = _netIDs;
        }

        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            _packet_handler.m_SectorChange.Invoke(m_NetIDs);
        }
    }

    #endregion

    #region OBJECT

    // TODO 오브젝트 상호작용
    public class ObjectInteractPacket : Packet_Base
    {
        int m_PlayerNetID;
        int m_ObjectID;
        int m_State;
        bool m_Instantly;

        public ObjectInteractPacket(int _playerNetID, int _objectID, int _state, bool _instantly)
        {
            m_PlayerNetID = _playerNetID;
            m_ObjectID = _objectID;
            m_State = _state;
            m_Instantly = _instantly;
        }

        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            _packet_handler.m_Interact.Invoke(m_PlayerNetID, m_ObjectID, m_State, m_Instantly);
        }
    }

    #endregion

    #region PLAYER

    // TODO 활성화
    public class PlayerEnablePacket : Packet_Base
    {
        int m_NetID;
        bool m_Enable;

        public PlayerEnablePacket(int _netID, bool _enable)
        {
            m_NetID = _netID;
            m_Enable = _enable;
        }

        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            _packet_handler.p_PlayerEnable.Invoke(m_NetID, m_Enable);
        }
    }

    // TODO ☆ 이동
    public class PlayerMovePacket : Packet_Base
    {
        int m_NetID;
        Vector3 m_Pos;
        Vector2 m_Axis;

        public PlayerMovePacket(int _netID, Vector3 _pos, Vector2 _axis)
        {
            m_NetID = _netID;
            m_Pos = _pos;
            m_Axis = _axis;
        }

        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
            // Player_Move에 뭐가 많이 있어서 ㅎㅎ 걍 놔둠
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            Ingame_Packet_Handler.Instance.p_Move.Invoke(m_NetID, m_Pos, m_Axis);
        }
    }

    // TODO ☆ 시선
    public class PlayerLookPacket : Packet_Base
    {
        int m_NetID;
        float m_Pitch;
        float m_Yaw;

        public PlayerLookPacket(int _netID, float _pitch, float _yaw)
        {
            m_NetID = _netID;
            m_Pitch = _pitch;
            m_Yaw = _yaw;
        }

        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
            // 이곳에 메소드 입력
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            _packet_handler.p_Look.Invoke(m_NetID, m_Pitch, m_Yaw);
        }
    }

    // TODO 꿇기
    public class PlayerCrouchPacket : Packet_Base
    {
        int m_NetID;
        bool m_Crouch;

        public PlayerCrouchPacket(int _netID, bool _bool)
        {
            m_NetID = _netID;
            m_Crouch = _bool;
        }

        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
            // 서버에게 현재 플레이어의 꿇기 값을 전달
            _network_manager.PackProtocol(ref _protocol, (UInt64)InGameManagerProtocol.CROUCH);
            _network_manager.S_BoolData(_protocol, false);
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            Ingame_Packet_Handler.Instance.p_Crouch.Invoke(m_NetID, m_Crouch);
        }
    }

    // TODO 달리기
    public class PlayerDashPacket : Packet_Base
    {
        int m_NetID;
        bool m_Dash;

        public PlayerDashPacket(int _netID, bool _bool)
        {
            m_NetID = _netID;
            m_Dash = _bool;
        }

        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
            _network_manager.PackProtocol(ref _protocol, (UInt64)InGameManagerProtocol.DASH);
            _network_manager.S_BoolData(_protocol, true);
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            Ingame_Packet_Handler.Instance.p_Dash.Invoke(m_NetID, m_Dash);
        }
    }

    // TODO 아이템 사용
    public class PlayerUseItemPacket : Packet_Base
    {
        int m_NetID;
        int m_ItemID;
        bool m_Dash;

        public PlayerUseItemPacket(int _netID, int _itemID, bool _bool)
        {
            m_NetID = _netID;
            m_ItemID = _itemID;
            m_Dash = _bool;
        }

        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
            // TODO 서버에게 아이템 쓴다고 전달
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            Ingame_Packet_Handler.Instance.p_UseItem.Invoke(m_NetID, m_ItemID, m_Dash);
        }
    }

    #endregion

    #region CHARACTER_GHOST
    // 고스트들 소환될 때마다 각자 등록

    // TODO 고스트 이동
    public class GhostMovePacket : Packet_Base
    {
        int m_GhostID;
        Vector3 m_Coord;
        float m_Time;

        public GhostMovePacket(int _ghostID, Vector3 _coord, float _time)
        {
            m_GhostID = _ghostID;
            m_Coord = _coord;
            m_Time = _time;
        }

        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            _packet_handler.e_GhostMove.Invoke(m_GhostID, m_Coord, m_Time);
        }
    }

    // TODO 고스트 충돌
    public class GhostHitPacket : Packet_Base
    {
        int m_NetID;
        int m_GhostID;

        public GhostHitPacket(int _netID, int _ghostID)
        {
            m_NetID = _netID;
            m_GhostID = _ghostID;
        }

        public override void Send(NetworkManager _network_manager, ref ulong _protocol)
        {
        }

        public override void Recv(Ingame_Packet_Handler _packet_handler)
        {
            _packet_handler.e_GhostHit.Invoke(m_NetID, m_GhostID);
        }
    }

    #endregion

}
