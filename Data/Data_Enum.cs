﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * 190905
 * 열거형 자료들
 */
namespace Malaia.Data
{
    #region LOBBY

    public enum LOBBY_STATE { WAITING, FULL, PLAYING }

    #endregion

    #region INTERACT

    public enum INTERACT_DESCTYPE { NONE, DOOR, WINDOW, LAMP }

    public enum ITEM_INDEX
    {
        NONE = 0,
        EQUIP_START = 1000,
            FLASHLIGHT = 1001,
            CAM = 1002,
        CONSUME_START = 2000,
            BREAD = 2001,
            MILK = 2002
    }

    #endregion

    #region PLAYER_STATE

    public enum STATE_UPPER { NONE }
    public enum STATE_LOWER { NONE, ON_GROUND, ON_AIR, CROUCH }

    public enum HUD_STATE { NORMAL, PHONE, CAMCODER }

    #endregion


}
