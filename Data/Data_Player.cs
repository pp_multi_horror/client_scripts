﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Malaia.Data
{
    public class Data_Player
    {
        /// <summary> 클라이언트 자신의 데이터 </summary>
        static Data_Player Client;

        #region LOBBY

        public int m_PlayerNetID; // 플레이어의 네트워크 ID
        public string m_Name; // 플레이어의 닉네임 최대 길이는 8
        public bool m_Lobby_GetReady; // 플레이어의 로비 내 레디 상태
        public int m_Current_LobbyID = -1; // 현재 접속한 로비의 ID, 기본값은 -1

        #endregion

        #region INGAME

        public bool m_Ingame_Is_Phantom = false; // 얘가 귀신인가
        public bool m_Ingame_Loading_Complete = false; // 인게임 로딩 상태
        public bool m_Ingame_Movable = false; // 현재 움직일 수 있는 상태인가

        public bool m_Ingame_Crouch = false; // 현재 꿇어앉은 상태인가
        public bool m_Ingame_Dash = false; // 현재 달리는 상태인가

        public ITEM_INDEX[] m_Ingame_Items = { 0, 0 };       // 소유 소모품 (1~2번)
        public ITEM_INDEX m_Ingame_Equipment = 0;   // 소유 장비 (3번)

        public bool m_Ingame_Equipment_Using = false; // 현재 장비를 사용중

        #endregion

        public static Data_Player GetClientData()
        {
            if (Client == null)
                Client = new Data_Player();
            return Client;
        }
        
    }
}
