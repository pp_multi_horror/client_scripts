﻿using System;
using UnityEngine;

namespace Malaia.Data
{
    public delegate void Item_Function(ref Data_Player _data, bool _activate);

    /// <summary>
    /// 190908
    /// 아이템 자료 클래스
    /// </summary>
    class Data_Item
    {
        public static GameObject Equip_FlashLight = null;
        public static GameObject Equip_Camcoder = null;
        public static GameObject Consume_Milk = null;
        public static GameObject Consume_Bread = null;

        /// <summary>
        /// 
        /// </summary>
        public static void Initialize()
        {
            Equip_FlashLight = Resources.Load<GameObject>("Prefab/Objects/Items/Equip_Flashlight");
            Equip_Camcoder = Resources.Load<GameObject>("Prefab/Objects/Items/Container");

            Consume_Bread = Resources.Load<GameObject>("Prefab/Objects/Items/Item_Bread");
            Consume_Milk = Resources.Load<GameObject>("Prefab/Objects/Items/Item_Milk_2");
        }

        public static GameObject Get_Item_Prefab(ITEM_INDEX _index)
        {
            switch(_index)
            {
                case ITEM_INDEX.FLASHLIGHT:
                    return Equip_FlashLight;
                case ITEM_INDEX.CAM:
                    return Equip_Camcoder;
                case ITEM_INDEX.BREAD:
                    return Consume_Bread;
                case ITEM_INDEX.MILK:
                    return Consume_Milk;
            }
            return null;
        }

        public static Item_Function Get_Item_Effect(ITEM_INDEX _index)
        {
            switch (_index)
            {
                case ITEM_INDEX.FLASHLIGHT:
                    return (ref Data_Player _data, bool _activate) =>
                    {
                        Window_Ingame_Loading.Instance.Add_String("장비 사용!");
                        _data.m_Ingame_Equipment_Using = _activate;
                    };
                case ITEM_INDEX.CAM:
                    return (ref Data_Player _data, bool _activate) =>
                    {
                        Window_Ingame_Loading.Instance.Add_String("장비 사용!");
                        _data.m_Ingame_Equipment_Using = _activate;
                    };
                case ITEM_INDEX.BREAD:
                    return (ref Data_Player _data, bool _activate) =>
                    {
                        Window_Ingame_Loading.Instance.Add_String("빵 먹기!");
                    };
                case ITEM_INDEX.MILK:
                    return (ref Data_Player _data, bool _activate) =>
                    {
                        Window_Ingame_Loading.Instance.Add_String("우유 마시기!");
                    };
            }
            return null;
        }

    }
}
