﻿using UnityEngine;
using UnityEngine.Events;

/*
 * 이벤트 자료들
 */
namespace Malaia.Data
{

    #region GAME PLAY
    // 시스템, 게임 진행

    [System.Serializable]
    public class SystemPingEvent : UnityEvent { }

    [System.Serializable]
    public class SystemGameStartEvent : UnityEvent { }

    #endregion

    #region MAP
    // 맵 관련

    [System.Serializable]
    public class PlayerSpawnEvent : UnityEvent<bool, int, Vector3> { }

    [System.Serializable]
    public class GhostSpawnEvent : UnityEvent<int, Vector3> { }

    [System.Serializable]
    public class GhostDeSpawnEvent : UnityEvent<int> { }

    [System.Serializable]
    public class ItemSpawnEvent : UnityEvent<Vector3, Vector3, int, int> { }

    /// <summary>
    /// 클라이언트에게 현재 활성화된 플레이어 리스트를 보내줌
    /// </summary>
    [System.Serializable]
    public class SectorChangeEvent : UnityEvent<int[]> { }

    #endregion

    #region OBJECT

    [System.Serializable]
    public class ObjectInteractEvent : UnityEvent<int, int, int, bool> { }

    // TODO 아이템 드롭
    /// <summary>
    /// 플레이어 아이템 드롭
    /// </summary>
    [System.Serializable]
    public class ItemDropEvent : UnityEvent<int, int, int> { }

    #endregion

    #region PLAYER
    // 플레이어 관련

    /// <summary>
    /// 플레이어 개체의 On/Off 를 설정
    /// </summary>
    [System.Serializable]
    public class PlayerEnableEvent : UnityEvent<int, bool> { }

    [System.Serializable]
    public class PlayerMoveEvent : UnityEvent<int, Vector3, Vector2> { }

    [System.Serializable]
    public class PlayerLookEvent : UnityEvent<int, float, float> { }

    [System.Serializable]
    public class PlayerCrouchEvent : UnityEvent<int, bool> { }

    [System.Serializable]
    public class PlayerDashEvent : UnityEvent<int, bool> { }

    [System.Serializable]
    public class PlayerUseItemEvent : UnityEvent<int, int, bool> { }
    

    #endregion

    #region GHOST
    // 유령 관련

    [System.Serializable]
    public class GhostMoveEvent : UnityEvent<int, Vector3, float> { }

    [System.Serializable]
    public class GhostHitEvent : UnityEvent<int, int> { }

    #endregion

    


}
