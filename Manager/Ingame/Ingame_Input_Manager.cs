﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Malaia.Data;
using Malaia.Packet;

/// <summary>
/// 인게임 입력 관리자
/// 서버에서 들어오는 입력을 그대로 재현한다
/// 즉, 패킷이 하나라도 빠지면 안됨
/// </summary>
public class Ingame_Input_Manager : MonoBehaviour
{
    public static Ingame_Input_Manager Instance;

    /*
     * 플레이어 오브젝트들은 자신이 생성/제거될 때 이곳에 자신의 메소드들을 등록/해제한다
     * 서버에서는 그냥 Recv 메소드에 밀어넣기만 하면 오브젝트들이 알아서 판단함
     * 클라이언트 자신의 오브젝트는 패킷을 보내면서 내부에서 스스로 움직인다
     * 즉 클라가 보낸 패킷은 심한 오차가 생기지 않는 한 클라가 다시 받아선 안됨
     * 클라의 움직임을 제한하려면 서버에서 나름대로의 규칙을 세우셈(16ms에 0.5m범위까지 움직일 수 있는데 2m를 움직이고서 패킷을 보냈다면 부정 조작으로 판단한다던지)
     * 이런 규칙을 만드려면 클라측에서도 자신의 타임스탬프 값을 보낼 필요가 있음(Ingame_Manager.Instance.m_Current_Time)
     */


    // 무시
    // public KeyEvent m_LeftClick;
    // public KeyEvent m_RightClick;

    public UnityEvent m_Menu;

    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);

        Instance = this;
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    private void Update()
    {
        if (Player.ClientPlayer == null)
            return;
        Player player = Player.ClientPlayer;
        int net_id = Data_Player.GetClientData().m_PlayerNetID;

        UInt64 protocol;
        protocol = 0;
        NetworkManager.Instance.PackProtocol(ref protocol, (UInt64)InGameManagerProtocol.MOVE);

        // 꿇기 처리
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            Ingame_Packet_Handler.Instance.Send(
                new PlayerCrouchPacket(net_id, true));

            /*
            Debug.Log("꿇었다.");
            Ingame_Packet_Handler.Instance.p_Crouch.Invoke(net_id, true);
            // 서버에게 현재 플레이어의 꿇기 값을 전달
            NetworkManager.Instance.PackProtocol(ref protocol, (UInt64)InGameManagerProtocol.CROUCH);
            NetworkManager.Instance.S_BoolData(protocol, true);
            */
        }
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            Ingame_Packet_Handler.Instance.Send(
                new PlayerCrouchPacket(net_id, false));

            /*
            Debug.Log("섰다.");
            Ingame_Packet_Handler.Instance.p_Crouch.Invoke(net_id, false);
            // 서버에게 현재 플레이어의 꿇기 값을 전달
            NetworkManager.Instance.PackProtocol(ref protocol, (UInt64)InGameManagerProtocol.CROUCH);
            NetworkManager.Instance.S_BoolData(protocol, false);
            */
        }

        // 달리기 처리
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            Ingame_Packet_Handler.Instance.Send(
                new PlayerDashPacket(net_id, true));

            /*
            Debug.Log("달린다.");
            Ingame_Packet_Handler.Instance.p_Dash.Invoke(net_id, true);
            // 서버에게 현재 플레이어의 달리기 값을 전달
            NetworkManager.Instance.PackProtocol(ref protocol, (UInt64)InGameManagerProtocol.DASH);
            NetworkManager.Instance.S_BoolData(protocol, true);
            */
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            Ingame_Packet_Handler.Instance.Send(
                new PlayerDashPacket(net_id, false));

            /*
            Debug.Log("걷는다.");
            Ingame_Packet_Handler.Instance.p_Dash.Invoke(net_id, false);
            // 서버에게 현재 플레이어의 달리기 값을 전달
            NetworkManager.Instance.PackProtocol(ref protocol, (UInt64)InGameManagerProtocol.DASH);
            NetworkManager.Instance.S_BoolData(protocol, false);
            */
        }

        // 아이템 사용 처리
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Ingame_Packet_Handler.Instance.Send(
                new PlayerUseItemPacket(net_id, 1, true));
            /*
            Debug.Log("템사용");
            Ingame_Packet_Handler.Instance.p_UseItem.Invoke(net_id, 1, true);
            */
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Ingame_Packet_Handler.Instance.Send(
                new PlayerUseItemPacket(net_id, 2, true));
            /*
            Debug.Log("템사용");
            Ingame_Packet_Handler.Instance.p_UseItem.Invoke(net_id, 2, true);
            */
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Ingame_Packet_Handler.Instance.Send(
                new PlayerUseItemPacket(net_id, 3, true));
            /*
            Debug.Log("장비사용");
            Ingame_Packet_Handler.Instance.p_UseItem.Invoke(net_id, 3, !Data_Player.GetClientData().m_Ingame_Equipment_Using);
            */
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Player.ClientPlayer == null)
            return;
        Player player = Player.ClientPlayer;
        int net_id = Data_Player.GetClientData().m_PlayerNetID;

        // 플레이어 이동 처리
        Vector3 pos = Player.ClientPlayer.transform.position;
        Vector2 axis = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Ingame_Packet_Handler.Instance.p_Move.Invoke(net_id, pos, axis);
    }
}
