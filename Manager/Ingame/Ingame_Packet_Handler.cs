﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Malaia.Data;
using Malaia.Packet;

/// <summary>
/// 190908
/// 인게임 패킷 받기 클래스
/// 모든 패킷은 받으면 그냥 Invoke(인자값) 시킬 것
/// </summary>
public class Ingame_Packet_Handler : MonoBehaviour
{
    public static Ingame_Packet_Handler Instance;

    // Properties
    Map m_Map;
    Ingame_Manager m_Ingame;
    PacketManager packet;

    // Members
    Queue<Packet_Base> m_Send_Queue; // 보내기 큐
    Queue<Packet_Base> m_Recv_Queue; // 받기 큐

    #region EVENTS

    #region INGAME_MANAGER
    // 인게임 매니저가 등록

    public SystemPingEvent e_Ping;
    public SystemGameStartEvent e_GameStart;

    #endregion

    #region MAP
    // 맵이 등록

    public PlayerSpawnEvent m_PlayerSpawn; // 플레이어 소환
    public GhostSpawnEvent m_GhostSpawn; // 고스트 소환
    public GhostDeSpawnEvent m_GhostDeSpawn; // 고스트 제거
    public ItemSpawnEvent m_ItemSpawn; // 아이템 소환

    public SectorChangeEvent m_SectorChange; // 섹터 변경에 따른 플레이어 On/Off
    
    #endregion

    #region OBJECT
    // 오브젝트들 알아서 등록

    public ObjectInteractEvent m_Interact; // 상호작용

    #endregion

    #region PLAYER
    // 플레이어들 소환될 때마다 각자 등록

    public PlayerEnableEvent p_PlayerEnable; // 플레이어 On/Off 이벤트
    public PlayerMoveEvent p_Move; // 이동
    public PlayerLookEvent p_Look; // 보기
    public PlayerCrouchEvent p_Crouch; // 숙이기
    public PlayerDashEvent p_Dash; // 달리기
    public PlayerUseItemEvent p_UseItem; // 아이템 사용

    #endregion

    #region CHARACTER_GHOST
    // 고스트들 소환될 때마다 각자 등록

    public GhostMoveEvent e_GhostMove;
    public GhostHitEvent e_GhostHit;

    #endregion

    #endregion

    private void Awake()
    {
        Instance = this;
        packet = new PacketManager();
    }


    public Ingame_Packet_Handler()
    {
        // 보내기, 받기 큐 초기화
        m_Send_Queue = new Queue<Packet_Base>();
        m_Recv_Queue = new Queue<Packet_Base>();
    }

    IEnumerator Start()
    {
        while (Map.Instance == null)
            yield return new WaitForEndOfFrame();
        while(Ingame_Manager.Instance == null)
            yield return new WaitForEndOfFrame();

        m_Map = Map.Instance;
        m_Ingame = Ingame_Manager.Instance;

        if (thread == null)
        {
            thread = new Thread(RunThread);
            thread.Start();
        }
        yield return null;
    }

    public void Update()
    {
        while (TaskQueue.Count > 0)
        {
            Task task = new Task();

            task.buffer = TaskQueue.Dequeue();

            UInt64 protocol = 0;

            packet.GetProtocol(task.buffer, ref protocol);

            Debug.Log("프로토콜 : " + protocol);

            bool isPhantom;
            int NetID;
            float pitch;
            float yaw;
            Vector3 pos;
            Vector2 axis;
            int[] PlayerListNetID;
            int dataSize;
            bool flag;
            int objectID = 0;
            int activate = 0;
            bool instantly = false;

            switch (NetworkManager.Instance.GameState_UnPackProtocol(protocol))
            {
                case InGameManagerProtocol.GAME_INIT:
                    break;

                case InGameManagerProtocol.PING:
                    e_Ping.Invoke();
                    break;

                case InGameManagerProtocol.GAME_PLAYER_DATA:
                    isPhantom = false;
                    NetID = 0;
                    pos = Vector3.zero;

                    //net.R_Player_Data(task.buffer, ref isPhantom, ref NetID, ref pos);
                    NetworkManager.Instance.R_Player_Data(task.buffer, ref isPhantom, ref NetID, ref pos);

                    Ingame_Packet_Handler.Instance.m_PlayerSpawn.Invoke(isPhantom, NetID, new Vector3(pos.x, pos.y, pos.z));
                    break;

                case InGameManagerProtocol.GAME_START:
                    e_GameStart.Invoke();
                    //Recv_Players_Loading_Complete();
                    break;

                case InGameManagerProtocol.MOVE:
                    if ((protocol & (UInt64)InGameManagerProtocol.POSITION) == (UInt64)InGameManagerProtocol.POSITION)
                    {

                        NetID = 0;
                        dataSize = 0;
                        pos = Vector3.zero;
                        axis = Vector2.zero;

                        NetworkManager.Instance.R_Position(task.buffer, ref NetID, ref pos, ref axis, ref dataSize);

                        Ingame_Packet_Handler.Instance.p_Move.Invoke(NetID, pos, axis);
                        //Ingame_Input_Manager.Instance.Recv_PlayerMove(NetID, pos, axis);

                        if ((protocol & (UInt64)InGameManagerProtocol.ROTATION) == (UInt64)InGameManagerProtocol.ROTATION)
                        {
                            NetID = 0;
                            pitch = 0;
                            yaw = 0;

                            NetworkManager.Instance.R_Rotation(task.buffer, ref NetID, ref pitch, ref yaw, dataSize);

                            Ingame_Packet_Handler.Instance.p_Look.Invoke(NetID, pitch, yaw);
                            //Ingame_Input_Manager.Instance.Recv_PlayerLook(NetID, pitch, yaw);
                        }

                        break;
                    }

                    if ((protocol & (UInt64)InGameManagerProtocol.ROTATION) == (UInt64)InGameManagerProtocol.ROTATION)
                    {
                        NetID = 0;
                        pitch = 0;
                        yaw = 0;

                        NetworkManager.Instance.R_Rotation(task.buffer, ref NetID, ref pitch, ref yaw);

                        Ingame_Packet_Handler.Instance.p_Look.Invoke(NetID, pitch, yaw);
                        //Ingame_Input_Manager.Instance.Recv_PlayerLook(NetID, pitch, yaw);
                    }

                    if ((protocol & (UInt64)InGameManagerProtocol.DASH) == (UInt64)InGameManagerProtocol.DASH)
                    {
                        NetID = 0;
                        flag = false;

                        NetworkManager.Instance.R_Dash(task.buffer, ref NetID, ref flag);

                        Ingame_Packet_Handler.Instance.p_Dash.Invoke(NetID, flag);
                        //Ingame_Input_Manager.Instance.Recv_PlayerDash(NetID, flag);
                    }

                    if ((protocol & (UInt64)InGameManagerProtocol.CROUCH) == (UInt64)InGameManagerProtocol.CROUCH)
                    {
                        NetID = 0;
                        flag = false;

                        NetworkManager.Instance.R_Crouch(task.buffer, ref NetID, ref flag);

                        Ingame_Packet_Handler.Instance.p_Crouch.Invoke(NetID, flag);
                        //Ingame_Input_Manager.Instance.Recv_PlayerCrouch(NetID, flag);
                    }
                    break;

                case InGameManagerProtocol.ACTIVE_OBJ:
                    PlayerListNetID = new int[10];

                    NetworkManager.Instance.ActiveObj(task.buffer, ref PlayerListNetID);

                    Ingame_Packet_Handler.Instance.m_SectorChange.Invoke(PlayerListNetID);
                    //Recv_Sector_State(PlayerListNetID);

                    break;

                case InGameManagerProtocol.UPDATE_USER_LIST:
                    NetworkManager.Instance.SendProtocol((UInt64)InGameManagerProtocol.SECTOR_LIST);
                    break;

                case InGameManagerProtocol.OBJECT:
                    // 2019_08_26(CHOI_작업)
                    NetID = 0;
                    Debug.Log("오브젝트");
                    NetworkManager.Instance.R_Object(task.buffer, ref NetID, ref objectID, ref activate, ref instantly);
                    Ingame_Packet_Handler.Instance.m_Interact.Invoke(NetID, objectID, activate, instantly);
                    break;

            }

        }
    }


    // TODO 패킷 뭐시기 필독
    /*
     * <패킷>
     * 패킷은 추상 클래스로 모든 패킷이 Send()와 Recv()를 가짐
     * Send안에는 패키징과정이 담김. 네트워크 매니저와 프로토콜을 인자값으로 요구
     * Recv시에는 클라이언트에서 그것을 시뮬레이션함. 패킷 핸들러만 인자값으로 요구함
     * 만약 더 처리해야 할 사항이 있는 경우에는 알아서 바꿔주세요
     * 
     * <패킷을 보내려면>
     * 송신하고자 하는 클래스 측에서 Ingame_Packet_Handler.Instance 를 얻기
     * 패킷핸들러의 Send( new 패킷뭐시기(인자값) ); 메소드를 통해 패킷 넣기
     * 이렇게 되면 패킷핸들러의 Send_Queue가 채워지게 됨
     * 이후 루프 함수라던지 돌아갈 때 Send_All_Packet() 메소드로 충전된 큐를 비워주면 됨
     * 
     * <패킷을 받는다면>
     * 모루겟소요
     * 일단 리시버 측에서 데이터를 받으면, 그것을 패킷으로 가공해야함
     * 이후 패킷핸들러의 인스턴스를 얻고, Recv( new 패킷뭐시기(인자값) ); 으로 Recv_Queue를 채웁니다
     * 보낼 때처럼 루프 함수 돌 때 Recv_All_Packet() 하면 차례대로 시뮬레이트!
     */


    /// <summary>
    /// 190918 JH
    /// 보내기 큐에 패킷 넣기
    /// </summary>
    /// <param name="_packet"></param>
    public void Send(Packet_Base _packet)
    {
        m_Send_Queue.Enqueue(_packet);
    }

    /// <summary>
    /// 190920 JH
    /// 받기 큐에 패킷 넣기
    /// </summary>
    /// <param name="_packet"></param>
    public void Recv(Packet_Base _packet)
    {
        m_Recv_Queue.Enqueue(_packet);
    }

    /// <summary>
    /// 190918 JH
    /// 패킷 전부 보내버리기
    /// </summary>
    public void Send_All_Packets()
    {
        UInt64 protocol;
        protocol = 0;

        // 센드 큐가 빌 때까지 반복
        while (m_Send_Queue.Peek() != null)
        {
            Packet_Base temp_packet = m_Send_Queue.Dequeue();
            temp_packet.Send(NetworkManager.Instance, ref protocol); // 패킷한테 인자값만 주고 알아서 하라고 하기
        }
    }

    /// <summary>
    /// 190920 JH
    /// 받은 패킷 전부 처리하기
    /// </summary>
    public void Recv_All_Packets()
    {
        // 리시브 큐가 빌 때까지 반복
        while (m_Recv_Queue.Peek() != null)
        {
            Packet_Base temp_packet = m_Recv_Queue.Dequeue();
            temp_packet.Recv(this);
        }
    }


    #region Thread

    private Thread thread = null;

    private object lockObject = new object();       // 임계영역 처리를 위한 오브젝트 생성
    private static Queue<byte[]> TaskQueue = new Queue<byte[]>();

    private class Task
    {
        public byte[] buffer = new byte[1024];
    }

    // 쓰레드 함수 구현
    void RunThread()
    {
        TaskQueue.Clear();

        while (true)
        {
            Task task = new Task();

            NetworkManager.Instance.PacketRecv(ref task.buffer);
            // 처리
            TaskQueue.Enqueue(task.buffer);
        }
    }

    #endregion

}
