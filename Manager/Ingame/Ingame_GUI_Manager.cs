﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Malaia.Data;

public class Ingame_GUI_Manager : MonoBehaviour
{
    public static Ingame_GUI_Manager Instance;
    public Window_Ingame_Loading m_Ingame_Loading; // 인게임 로딩 HUD (디버그)
    
    public HUD_STATE m_Current_State; // 현재 GUI 상태

    public GameObject m_Normal_HUD; // 보통의 HUD
    public Element_Supply[] m_GUI_Supplies; // 소모품 창
    public Element_Supply m_GUI_Equipment; // 장비 창

    public GameObject m_Phone_HUD; // 핸드폰 HUD

    public Camera m_Cam_Camera; // 캠코더 전용 카메라
    public GameObject m_Cam_HUD; // 캠코더 HUD
    public CameraFilterPack_FX_Glitch3 m_Cam_Glitch; // 캠코더 글리치 수준

    public Text m_Interact_Notice; // 상호작용 문구
    public RectTransform m_Ghost_JumpScare; // 기싱 이미지

    int base_culling_mask;

    void Awake()
    {
        Instance = this;
        base_culling_mask = Camera.main.cullingMask;
    }

    void OnDestroy()
    {
        Instance = null;
    }

    /// <summary>
    /// 190904
    /// 카메라 HUD 변경
    /// </summary>
    public void Update_HUD(HUD_STATE _state)
    {
        m_Current_State = _state;

        m_Normal_HUD.SetActive(false);
        m_Phone_HUD.SetActive(false);
        m_Cam_HUD.SetActive(false);
        m_Cam_Camera.gameObject.SetActive(false);

        // 일반 상태 화면
        if (m_Current_State == HUD_STATE.NORMAL)
        {
            m_Normal_HUD.SetActive(true);
        }

        // 폰 화면
        if (m_Current_State == HUD_STATE.PHONE)
        {
            m_Normal_HUD.SetActive(true);
            m_Phone_HUD.SetActive(true);
        }

        // 캠 화면
        if (m_Current_State == HUD_STATE.CAMCODER)
        {
            m_Cam_HUD.SetActive(true);
            m_Cam_Camera.gameObject.SetActive(true);
        }

        // 화면 선택 종류 후 처리
        Camera.main.cullingMask = base_culling_mask + (m_Current_State == HUD_STATE.CAMCODER ? (1 << 11) : 0);
    }

    /// <summary>
    /// 190908
    /// 툴바 업데이트
    /// </summary>
    public void Update_Toolbar()
    {
        Data_Player data = Data_Player.GetClientData();
        for(int i = 0; i < m_GUI_Supplies.Length; i++)
            m_GUI_Supplies[i].Update_Supply_Image((int)data.m_Ingame_Items[i] - 2001);
        m_GUI_Equipment.Update_Supply_Image((int)data.m_Ingame_Equipment - 1001);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Update_HUD(m_Current_State == HUD_STATE.PHONE ? HUD_STATE.NORMAL : HUD_STATE.PHONE);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Update_HUD(m_Current_State == HUD_STATE.CAMCODER ? HUD_STATE.NORMAL : HUD_STATE.CAMCODER);
        }

        // 글리치 업데이트
        if (m_Current_State == HUD_STATE.CAMCODER)
        {
            float shortest_dist = 8f;
            foreach(Character_Ghost ghost in Ingame_Manager.Instance.m_Map.m_Ghosts)
            {
                float temp_dist = Vector3.Distance(MainCam.Instance.transform.position, ghost.transform.position);
                if (temp_dist < 8f && temp_dist < shortest_dist)
                    shortest_dist = temp_dist;
            }
            m_Cam_Glitch._Glitch = (1f - shortest_dist / 8f) * 0.4f;
        }
    }

    public void Finish_Loading()
    {
        //m_Ingame_Loading.gameObject.SetActive(false);
        m_Normal_HUD.SetActive(true);
    }

    public IEnumerator JumpScare()
    {
        m_Ghost_JumpScare.gameObject.SetActive(true);

        yield return new WaitForSeconds(1f);

        m_Ghost_JumpScare.gameObject.SetActive(false);

        yield return null;
    }
}
