﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.Events;
using Malaia.Data;
using Malaia.Packet;
using System.Net.Sockets;

/// <summary>
/// 인게임 총괄 클래스
/// </summary>
public class Ingame_Manager : MonoBehaviour
{
    public static Ingame_Manager Instance;
    public Data_Lobby m_Ingame_LobbyData; // 인게임 로비 데이터
    public Map m_Map; // 관리하고 있는 맵
    public Data_Ping m_Ping; // 현재 핑

    long m_Current_Time; // 게임이 시작되고나서의 시간


    #region 인게임 부팅 순서
    /*
     * 1. 서버가 플레이어들이 인간이 될지 귀신이 될지, 어느 좌표에 소환시킬지까지 미리 정해둔다
     *   1-1. 플레이어의 좌표는 (-5f~5f, 1f, -5f~5f) 사이에서 랜덤으로 배정
     *          인자값은 플레이어 NetID, Vector3 좌표
     *   1-2. 귀신쟝의 좌표는 (10f, 1f,  0f) 고정
     *          인자값은 플레이어 NetID, Vector3 좌표
     * 2. 클라는 로딩을 다 마친 뒤 인게임 씬에 진입한다
     * 3. 이후 서버와 핑을 10번 정도 주고 받는다
     * 4. 클라는 리소스 로딩도 끝났고, 서버와의 통신으로 핑 평균값도 얻었으므로 준비가 되었다는 신호를 보낸다
     * 5. 신호를 받은 서버는 클라에게 현재 준비된 모든 플레이어의 개체를 스폰시키도록 신호를 보낸다
     * 6. 다른 클라가 준비되면 서버가 해당 정보를 모든 클라에게 전달해 개체를 스폰시키게 한다
     * 7. 모든 플레이어의 로딩이 끝나면 모든 플레이어의 개체 또한 스폰되어있으므로 게임을 진행한다
    */
    #endregion

    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
        Data_Item.Initialize();
        m_Ping = new Data_Ping();

    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(1f);

        while (Window_Ingame_Loading.Instance == null)
            yield return new WaitForEndOfFrame();

        try
        {
            Initialize(Data_Lobby.Current_Lobby);
        }
        catch(Exception e)
        {
            Window_Ingame_Loading.Instance.Add_String(e.ToString());
        }

        Ingame_Packet_Handler iph = Ingame_Packet_Handler.Instance;
        iph.e_Ping.AddListener(new UnityAction(Ping));
        iph.e_GameStart.AddListener(new UnityAction(Loading_Complete));

        m_Ping.Start_Check();
        iph.Send(new SystemPingPacket()); // Ping 보내기, Pong 을 받기 위해

        yield return null;
    }

    public void Initialize(Data_Lobby _data)
    {
        m_Ingame_LobbyData = _data;
    }

    /// <summary>
    /// 게임 시작
    /// </summary>
    public void Start_Game()
    {
        // 자신의 객체를 찾아 그곳으로 캠을 옮긴다
        // 조작 가능한 상태에 돌입

        Ingame_GUI_Manager.Instance.Finish_Loading();
        for(int i = 0; i < m_Map.m_Players.Count; i++)
        {
            if(m_Map.m_Players[i].m_PlayerData.m_PlayerNetID == Data_Player.GetClientData().m_PlayerNetID)
            {
                m_Map.m_Players[i].m_Is_ClientCharacter = true;
                MainCam.Instance.Change_Camera_Parent(m_Map.m_Players[i]);
            }
            else
                m_Map.m_Players[i].m_Is_ClientCharacter = false;
            m_Map.m_Players[i].m_Move.Initialize();
        }
        MainCam.Instance.Lock_Cursor(true);
        Data_Player.GetClientData().m_Ingame_Movable = true;
        Object_Speaker.Play_SchoolBell(2, 0.6f);
    }

    private void Update()
    {
    }

    void FixedUpdate()
    {
        m_Current_Time += (long)(Time.fixedDeltaTime * 1000d); // 타임스탬프 더하기
    }


    #region EVENT METHOD

    /// <summary>
    /// 190908 - 이름 변경 Recv_Pong -> Ping
    /// 서버랑 그냥...... 그냥 신호만 주고 받음
    /// 이걸 10번 해서 평균값을 얻어야 로딩이 끝난 것으로 판단함
    /// </summary>
    public void Ping()
    {
        if (m_Ping.Update_Ping()) // 핑 3번 획득 완료
        {
            if (!Data_Player.GetClientData().m_Ingame_Loading_Complete) // 클라이언트의 레디 상태가 X인 경우
            {
                Window_Ingame_Loading.Instance.Add_String("너님의 로딩이 끝났어요.");
                Data_Player.GetClientData().m_Ingame_Loading_Complete = true;

                Debug.Log("Player Loading Complete");

                NetworkManager.Instance.Loading_Ready();
            }
        }
        StartCoroutine(Send_Ping());
    }

    IEnumerator Send_Ping()
    {
        yield return new WaitForSeconds(1f);
        m_Ping.Start_Check();
        NetworkManager.Instance.S_Ping();

        yield return null;
    }

    /// <summary>
    /// 190908 - 이름 변경 Recv_Players_Loading_Complete -> Loading_Complete
    /// 플레이어들의 로딩이 끝남
    /// 서버 기준 1000ms 뒤부터 게임을 시작하는데
    /// 서버에서 클라로 오는 시간이 있으므로 그 시간은 빠진 뒤에 게임이 시작됨
    /// 그 뒤로 타임스탬프를 계속 더해가면서 모든 걸 보정하게 됨
    /// </summary>
    public void Loading_Complete()
    {
        Window_Ingame_Loading.Instance.Add_String("모든 플레이어의 로딩이 끝나서 곧 게임을 시작해요.");
        StartCoroutine(Wait_For_Start(1000 - m_Ping.m_Average));
    }

    IEnumerator Wait_For_Start(int _time)
    {
        yield return new WaitForSecondsRealtime(_time * 0.001f);
        Start_Game();
        yield return null;
    }

    #endregion

}

