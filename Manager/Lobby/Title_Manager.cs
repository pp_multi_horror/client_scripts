﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
 *  190719
 */
public class Title_Manager : MonoBehaviour
{
    public enum MENU_STATE { MAIN, JOIN, LOBBYLIST, LOBBY }

    public static Title_Manager Instance;

    public GameObject TitleObject;
    public Text desc;
    public CameraFilterPack_Real_VHS vhs;

    public AudioClip WalkingSound;
    public AudioClip VCR_Turn;
    public AudioClip VCR_OK;

    public Window_Base Window_Title;
    public Window_Base Window_Join;
    public Window_Base Window_LobbyList;
    public Window_Base Window_Lobby;

    Window_Base m_Current_Window;
    public MENU_STATE m_State;
    AudioSource source;
    int cursor;
    bool button_ignore = false; 

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        source = GetComponent<AudioSource>();

        //source.clip = WalkingSound;
        //source.Play();

        yield return new WaitForSeconds(4f);
        
        Play_ClickSound();
        vhs.enabled = true;
        TitleObject.SetActive(true);
        Refresh_Window();

        yield return new WaitForSeconds(2f);
        desc.gameObject.SetActive(true);

        yield return null;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Current_Window == null) return;

        if (!button_ignore)
            m_Current_Window.Update_Input();
    }
    
    public void Refresh_Window()
    {
        if(m_Current_Window != null)
            m_Current_Window.gameObject.SetActive(false);
        switch (m_State)
        {
            case MENU_STATE.MAIN:
                m_Current_Window = Window_Title;
                break;
            case MENU_STATE.JOIN:
                m_Current_Window = Window_Join;
                break;
            case MENU_STATE.LOBBYLIST:
                m_Current_Window = Window_LobbyList;
                break;
            case MENU_STATE.LOBBY:
                m_Current_Window = Window_Lobby;
                break;
        }
        m_Current_Window.gameObject.SetActive(true);
        m_Current_Window.Initialize();
        m_Current_Window.Update_Text();
    }

    void Load_IngameScene()
    {
        SceneManager.LoadScene(1);
    }

    public void Play_ClickSound()
    {
        source.clip = VCR_Turn;
        source.Play();
    }

    public void Play_OKSound()
    {
        source.clip = VCR_OK;
        source.Play();
    }
}
