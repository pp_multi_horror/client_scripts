﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Malaia.Data;

/// <summary>
/// 최상위 관리자 클래스
/// </summary>
public class Master : MonoBehaviour
{
    public static Master Instance;

    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
}
