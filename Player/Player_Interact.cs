﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Malaia.Data;

/// <summary>
/// 플레이어 상호작용 담당 클래스
/// </summary>
public class Player_Interact
{
    Player m_Player;
    Text m_InteractNotice; // GUI 설명 텍스트, 나중에 매니저 통해서 바꿀 것

    Interactable_Object m_Cache_IO; // 캐시 콜라이더, 비교용

    public Player_Interact(Player _player, Text _notice_text)
    {
        m_Player = _player;
        m_InteractNotice = _notice_text;
    }

    public void Initialize()
    {
    }

    // Update is called once per frame
    public void Update()
    {
        if (m_Player.m_PlayerData == Data_Player.GetClientData())
        {
            Interactable_Object io = Get_InteractableCollider_With_Ray();
            if (io != null)
            {
                // 캐시 콜라이더와 현재 부딪힌 콜라이더가 다르다
                if (m_Cache_IO != io)
                {
                    m_Cache_IO = io;
                    m_InteractNotice.text = io.Get_Notice_Desc();
                }

                if (Input.GetKeyDown(KeyCode.E))
                    Command_Interact();
            }
            else
            {
                m_Cache_IO = null;
                if (m_InteractNotice != null)
                    m_InteractNotice.text = "";
            }
        }
    }

    /// <summary>
    /// 플레이어 상호작용 커맨드
    /// </summary>
    void Command_Interact()
    {
        UInt64 protocol = 0;
        Interactable_Object io = Get_InteractableCollider_With_Ray();
        if (io != null)
        {
            // 서버에게 m_Player.m_PlayerData.m_PlayerNetID 가 io.m_Object_ID 번째 오브젝트를 0으로 한다고 전달하기(일단은)
            // instantly도 일단은 false
            // 2019_08_26(CHOI_작업)
            NetworkManager.Instance.PackProtocol(ref protocol, (UInt64)InGameManagerProtocol.OBJECT);
            NetworkManager.Instance.S_Object(protocol, m_Player.m_PlayerData.m_PlayerNetID, io.m_Object_ID, 0, false);
            Ingame_Packet_Handler.Instance.m_Interact.Invoke(m_Player.m_PlayerData.m_PlayerNetID, io.m_Object_ID, 0, false);
        }
    }

    /// <summary>
    /// 카메라 중심으로 광선을 쏴서 닿는 곳에 위치한 콜라이더가 상호작용 가능한지 판별한다
    /// 상호작용 불가능한 콜라이더라면 NULL 반환
    /// </summary>
    /// <returns></returns>
    Interactable_Object Get_InteractableCollider_With_Ray()
    {
        // 카메라 중심 지점으로 광선 쏘기
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out RaycastHit hit, 1f); // 이 때 맞는 건 Trigger 체크 된 콜라이더

        // Interactable 태그 확인
        if (hit.collider != null && hit.collider.CompareTag("Interactable"))
        {
            Interactable_Object io = hit.collider.gameObject.GetComponent<Interactable_Object>();
            return io;
        }
        return null;
    }
}