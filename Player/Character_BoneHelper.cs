﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Malaia.Data;

public class Character_BoneHelper : MonoBehaviour
{
    public Transform Camera_Head;
    public Transform Seeing_Pos;
    public Transform Left_Hand;
    public Transform Right_Hand;

    public GameObject IK_Aim;
    public Transform[] Equipments;

    private void Update()
    {
        Ray ray = new Ray(Camera_Head.position, Camera_Head.forward);//  Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction);
        Physics.Raycast(ray, out RaycastHit hit, 200f); // 이 때 맞는 건 Trigger 체크 된 콜라이더
        if (hit.point != Vector3.zero)
            Seeing_Pos.position = hit.point;
        else
            Seeing_Pos.position = Camera_Head.position + Camera_Head.forward * 200f;
    }

    public void Update_Equipments(Data_Player _data)
    {
        IK_Aim.SetActive(_data.m_Ingame_Equipment_Using);
        for (int i = 0; i < Equipments.Length; i++)
            Equipments[i].gameObject.SetActive(_data.m_Ingame_Equipment_Using && (i == (int)_data.m_Ingame_Equipment - 1001));
    }

    public void Update_Seeing_Pos(float _pitch, float _yaw)
    {

    }
}
