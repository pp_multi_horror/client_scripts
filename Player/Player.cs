﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Malaia.Data;

/// <summary>
/// 플레이어 객체 클래스
/// 컨테이너 박스라는 느낌
/// </summary>
public class Player : MonoBehaviour
{
    public static Player ClientPlayer;

    public GameObject m_CoreObject;
    public GameObject m_HumanChara;
    public GameObject m_PhantomChara;

    public Data_Player           m_PlayerData;
    public Rigidbody            m_Rigidbody;
    public Animator             m_Animator;
    public Character_BoneHelper m_BoneHelper;

    public bool m_Is_ClientCharacter;
    public bool m_Is_Ready { private set; get; }

    public STATE_UPPER m_State_Upper; // 윗도리 상태
    public STATE_LOWER m_State_Lower; // 아랫도리 상태

    GameObject m_MainChara;
    Player_Interact     m_Interact;     // 상호작용
    public Player_Move  m_Move;      // 이동
    Ingame_GUI_Manager  m_IngameGUI; // GUI
    
    // Start is called before the first frame update
    IEnumerator Start()
    {
        // 로딩
        //while (Ingame_GUI_Manager.Instance == null)
        //    yield return new WaitForEndOfFrame();
        while (Ingame_Input_Manager.Instance == null)
            yield return new WaitForEndOfFrame();

        // 초기화
        m_IngameGUI = Ingame_GUI_Manager.Instance;
        m_Move = new Player_Move(this, m_Animator);
        m_Is_Ready = true;

        // 이벤트 등록
        Ingame_Packet_Handler.Instance.p_PlayerEnable.AddListener(Sector_Activate_Player);
        Ingame_Packet_Handler.Instance.p_UseItem.AddListener(Use_Item);

        // 이 플레이어 캐릭터가 클라이언트의 캐릭터인 경우
        if (m_PlayerData.m_PlayerNetID == Data_Player.GetClientData().m_PlayerNetID)
        {
            ClientPlayer = this;
            m_Interact = new Player_Interact(this, m_IngameGUI.m_Interact_Notice);
            foreach (Transform tr in m_CoreObject.GetComponentsInChildren<Transform>())
                tr.gameObject.layer = 10;
        }

        yield return null;
    }

    /// <summary>
    /// 캐릭터를 설정
    /// </summary>
    /// <param name="_is_phantom"></param>
    public void Set_Character(bool _is_phantom)
    {
        m_HumanChara.SetActive(!_is_phantom);
        m_PhantomChara.SetActive(_is_phantom);
        m_MainChara = _is_phantom ? m_PhantomChara : m_HumanChara;
        m_Animator = m_MainChara.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Is_Ready)
        {
            //Debug.Log("ID - " + m_PlayerData.m_PlayerNetID);
            if (m_Interact != null)
                m_Interact.Update(); // 상호작용 업데이트
            m_Move.Update();
        }
    }

    /// <summary>
    /// 190812
    /// 플레이어를 활성화 / 비활성화
    /// 외관만 사라지고 스크립트는 계속 돌아감
    /// </summary>
    /// <param name="_netID"></param>
    /// <param name="_activate"></param>
    public void Sector_Activate_Player(int _netID, bool _activate)
    {
        if (m_PlayerData.m_PlayerNetID == _netID)
        {
            m_CoreObject.SetActive(_activate);
        }
    }

    /// <summary>
    /// 190908
    /// 아이템을 사용
    /// </summary>
    /// <param name="_netID"></param>
    /// <param name="_slot"></param>
    /// <param name="_activate">activate는 장비 전용</param>
    public void Use_Item(int _netID, int _slot, bool _activate)
    {
        if (m_PlayerData.m_PlayerNetID == _netID)
        {
            ITEM_INDEX index = ITEM_INDEX.NONE;
            if (_slot < 3)
            {
                index = m_PlayerData.m_Ingame_Items[_slot - 1];
                m_PlayerData.m_Ingame_Items[_slot - 1] = ITEM_INDEX.NONE;
            }
            else
            {
                index = m_PlayerData.m_Ingame_Equipment;
            }
            Data_Item.Get_Item_Effect(index)?.Invoke(ref m_PlayerData, _activate); // delegate 를 호출, null이 아니면 실행

            m_BoneHelper.Update_Equipments(m_PlayerData);
            Ingame_GUI_Manager.Instance.Update_Toolbar();
        }
    }

}
