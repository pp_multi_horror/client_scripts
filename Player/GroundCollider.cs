﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GroundCollider : MonoBehaviour
{
    public UnityEvent m_GroundEnter;
    public UnityEvent m_GroundExit;

    private void OnTriggerEnter(Collider other)
    {
        m_GroundEnter.Invoke();
    }

    private void OnTriggerExit(Collider other)
    {
        m_GroundExit.Invoke();
    }
}
