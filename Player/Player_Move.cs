﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Malaia.Data;
using Malaia.Packet;

/// <summary>
/// 플레이어 이동 담당 클래스
/// </summary>
public class Player_Move
{
    Player player;
    Animator m_Animator;

    GroundCollider m_GroundCollider; // 땅 착지

    // float m_Current_VerticalSpeed = 0f; // 현재 이동속도 (수직)
    // float m_Current_HorizontalSpeed = 0f; // 현재 이동속도 (수평)

    float m_Crouch_MoveSpeed = 0.35f; // 꿇기 이동속도
    float m_Walk_MoveSpeed = 1.5f; // 걷기 이동속도
    float m_Dash_MoveSpeed = 5f; // 달리기 이동속도

    // float m_Speed_Acc = 2f; // 이동속도 가속력(1초에 어느정도)
    // float m_Speed_Dcc = 4f; // 이동속도 감속력(1초에 어느정도)
    float m_Anim_VerticalSpeed = 0f; // 애니메이션 수직 입력 인자값

    // Network
    Vector3 m_Final_Coord = Vector3.zero; // 마지막 기록 좌표
    Vector2 m_Final_Axis = Vector2.zero;
    Vector2 m_Final_Look = Vector2.zero; // 마지막 기록 보는 방향
    Vector2 m_Current_Look = Vector2.zero; // 현재 기록 보는 방향

    public Player_Move(Player _player, Animator _animator)
    {
        player = _player;
        m_Animator = _animator;
        m_GroundCollider = player.GetComponentInChildren<GroundCollider>();

        m_Anim_VerticalSpeed = 0f;

        if (m_GroundCollider != null)
        {
            m_GroundCollider.m_GroundEnter.AddListener(new UnityAction(GroundEnter));
            m_GroundCollider.m_GroundExit.AddListener(new UnityAction(GroundExit));
        }
    }

    public void Initialize()
    {
        Ingame_Packet_Handler IPH = Ingame_Packet_Handler.Instance;
        IPH.p_Move.AddListener(new UnityAction<int, Vector3, Vector2>(Update_Move));
        IPH.p_Look.AddListener(new UnityAction<int, float, float>(Update_Look));
        IPH.p_Crouch.AddListener(new UnityAction<int, bool>(Update_Crouch));
        IPH.p_Dash.AddListener(new UnityAction<int, bool>(Update_Dash));

        if (player.m_PlayerData == Data_Player.GetClientData())
        {
            player.StartCoroutine(Update_Coord());
        }
    }

    public void Update()
    {
        m_Animator.SetFloat("JumpSpeed", player.m_Rigidbody.velocity.y);

        Move();
        Animate();
    }

    /// <summary>
    /// 190812
    /// 좌표와 보는 방향을 업데이트, 일단은 0.3초마다
    /// </summary>
    /// <returns></returns>
    IEnumerator Update_Coord()
    {
        UInt64 protocol = 0;

        while (player != null)
        {
            protocol = 0;

            //NetworkManager.Instance.SendProtocol((UInt64)InGameManagerProtocol.SECTOR_LIST);

            // 좌표가 달라진 경우
            if (m_Final_Coord != player.transform.position)
            {
                NetworkManager.Instance.PackProtocol(ref protocol, (UInt64)InGameManagerProtocol.MOVE);
                NetworkManager.Instance.PackProtocol(ref protocol, (UInt64)InGameManagerProtocol.POSITION);

                m_Final_Coord = player.transform.position;

                // 포지션 + 시선값도 변경되었을 경우
                if (m_Final_Look != m_Current_Look)
                {
                    NetworkManager.Instance.PackProtocol(ref protocol, (UInt64)InGameManagerProtocol.ROTATION);

                    
                    m_Final_Look = m_Current_Look;

                    NetworkManager.Instance.S_Move(
                        protocol,
                        m_Final_Coord, new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")),
                        m_Final_Look);
                }
                // 좌표값만 변경되었을 경우
                else
                {
                    NetworkManager.Instance.S_Position(
                        protocol,
                        m_Final_Coord,
                        new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")));
                }
            }

            // 보는 시점이 달라진 경우
            // 시선값만 변경된 경우
            else if (m_Final_Look != m_Current_Look)
            {   
                NetworkManager.Instance.PackProtocol(ref protocol, (UInt64)InGameManagerProtocol.MOVE);
                NetworkManager.Instance.PackProtocol(ref protocol, (UInt64)InGameManagerProtocol.ROTATION);

                m_Final_Look = m_Current_Look;
                // 서버에게 netID, pitch, yaw 값을 전달
                Ingame_Packet_Handler.Instance.Send(new PlayerLookPacket(Data_Player.GetClientData().m_PlayerNetID, ));

                NetworkManager.Instance.S_Rotation(protocol, m_Final_Look);
            }

            yield return new WaitForSecondsRealtime(1.0f);
        }

        yield return null;
    }

    /// <summary>
    /// 190812
    /// 무브 이벤트, 플레이어의 좌표와 이동 방향을 설정
    /// </summary>
    /// <param name="_id">넷 ID</param>
    /// <param name="_coord">좌표</param>
    /// <param name="_dir">방향</param>
    void Update_Move(int _id, Vector3 _coord, Vector2 _dir)
    {
        if (player.m_PlayerData.m_PlayerNetID == _id)
        {
            player.transform.position = _coord;
            m_Final_Axis = _dir;
            Update_Player_Dir();
        }
    }

    /// <summary>
    /// 190812
    /// 플레이어의 보는 시점을 설정
    /// </summary>
    /// <param name="_id">넷 ID</param>
    /// <param name="_pitch">수직</param>
    /// <param name="_yaw">수평</param>
    void Update_Look(int _id, float _pitch, float _yaw)
    {
        if (player.m_PlayerData.m_PlayerNetID == _id)
        {
            player.m_BoneHelper.Camera_Head.localRotation = Quaternion.Euler(new Vector3(-_pitch, _yaw, 0f));
            m_Current_Look = new Vector2(_pitch, _yaw);

            if (m_Final_Axis != Vector2.zero)
                Update_Player_Dir();
        }
    }

    /// <summary>
    /// 190816
    /// 플레이어가 꿇는가 아닌가를 설정
    /// </summary>
    /// <param name="_id">넷 ID</param>
    /// <param name="_crouch">true 라면 꿇기</param>
    void Update_Crouch(int _id, bool _crouch)
    {
        if (player.m_PlayerData.m_PlayerNetID == _id)
        {
            player.m_PlayerData.m_Ingame_Crouch = _crouch;
            m_Animator.SetBool("OnCrouch", player.m_PlayerData.m_Ingame_Crouch);
        }
    }

    /// <summary>
    /// 190816
    /// 플레이어가 달리는가 아닌가를 설정
    /// </summary>
    /// <param name="_id">넷 ID</param>
    /// <param name="_crouch">true 라면 달리기</param>
    void Update_Dash(int _id, bool _dash)
    {
        if (player.m_PlayerData.m_PlayerNetID == _id)
        {
            player.m_PlayerData.m_Ingame_Dash = _dash;
        }
    }

    /// <summary>
    /// 190813
    /// 애니메이션 갱신
    /// </summary>
    void Animate()
    {
        m_Animator.SetFloat("Axis_Vertical", m_Anim_VerticalSpeed);
        m_Animator.SetFloat("Axis_Horizontal", m_Final_Axis.x);

        Vector3 cam_pos = player.m_BoneHelper.Camera_Head.localPosition;
        if (!player.m_PlayerData.m_Ingame_Crouch)
            player.m_BoneHelper.Camera_Head.localPosition = Vector3.Lerp(cam_pos, new Vector3(0f, 1.45f, 0f), 0.1f);
        else
            player.m_BoneHelper.Camera_Head.localPosition = Vector3.Lerp(cam_pos, new Vector3(0f, 0.7f, 0f), 0.1f);
    }

    /// <summary>
    /// 190812
    /// 매 업데이트시마다 마지막에 받은 방향을 향해 움직인다
    /// </summary>
    void Move()
    {
        if (m_Final_Axis == Vector2.zero)
            return;

        /*
        float to_speed_vertical = 0f; // 목표 속도 (수직)
        float to_speed_horizontal = 0f; // 목표 속도 (수평)

        // 수직
        if(m_Final_Axis.y < 0f) // 뒤로 갈 때
            to_speed_vertical = player.m_PlayerData.m_Ingame_Crouch ? m_Crouch_MoveSpeed : m_Walk_MoveSpeed;
        else if (m_Final_Axis.y > 0f) // 앞으로 갈 때
            to_speed_vertical = player.m_PlayerData.m_Ingame_Crouch ? m_Crouch_MoveSpeed : (player.m_PlayerData.m_Ingame_Dash ? m_Dash_MoveSpeed : m_Walk_MoveSpeed);
        to_speed_vertical *= m_Final_Axis.y;

        if(to_speed_vertical.Equals(0f))
            m_Current_VerticalSpeed = Mathf.MoveTowards(m_Current_VerticalSpeed, to_speed_vertical, m_Speed_Dcc * Time.deltaTime); // 감속
        else
            m_Current_VerticalSpeed = Mathf.MoveTowards(m_Current_VerticalSpeed, to_speed_vertical, m_Speed_Acc * Time.deltaTime); // 가속

        // 수평
        to_speed_horizontal = player.m_PlayerData.m_Ingame_Crouch ? m_Crouch_MoveSpeed : m_Walk_MoveSpeed;
        to_speed_horizontal *= m_Final_Axis.x;

        if (to_speed_horizontal.Equals(0f))
            m_Current_HorizontalSpeed = Mathf.MoveTowards(m_Current_HorizontalSpeed, to_speed_horizontal, m_Speed_Dcc * Time.deltaTime); // 감속
        else
            m_Current_HorizontalSpeed = Mathf.MoveTowards(m_Current_HorizontalSpeed, to_speed_horizontal, m_Speed_Acc * Time.deltaTime); // 가속

        // 이동
        float original_angle = player.m_BoneHelper.Camera_Head.transform.localRotation.eulerAngles.y;
        Vector3 dir = Calculate_Dir(original_angle, m_Final_Axis.x, m_Final_Axis.y, m_Current_HorizontalSpeed, m_Current_VerticalSpeed);
        player.m_Rigidbody.MovePosition(player.m_Rigidbody.position + (dir * Time.deltaTime));
        */

        float to_speed = 0f;
        if (!m_Final_Axis.x.Equals(0f) || m_Final_Axis.y < 0f) // 옆으로 가거나 뒤로 가는경우 대쉬 X
            to_speed = player.m_PlayerData.m_Ingame_Crouch ? m_Crouch_MoveSpeed : m_Walk_MoveSpeed;
        else
            to_speed = player.m_PlayerData.m_Ingame_Crouch ? m_Crouch_MoveSpeed : (player.m_PlayerData.m_Ingame_Dash ? m_Dash_MoveSpeed : m_Walk_MoveSpeed);
        to_speed *= Mathf.Max(Mathf.Max(m_Final_Axis.x, m_Final_Axis.y), Mathf.Min(m_Final_Axis.x, m_Final_Axis.y) * -1f);
        float original_angle = player.m_BoneHelper.Camera_Head.transform.localRotation.eulerAngles.y;
        Vector3 dir = Calculate_Dir(original_angle, m_Final_Axis.x, m_Final_Axis.y, 0f, 0f);
        player.m_Rigidbody.MovePosition(player.m_Rigidbody.position + (dir * to_speed * Time.deltaTime));

        m_Anim_VerticalSpeed = m_Final_Axis.y * to_speed / (player.m_PlayerData.m_Ingame_Crouch ? m_Crouch_MoveSpeed : m_Walk_MoveSpeed);
        //m_Animator.SetFloat("Speed", speed / m_Walk_MoveSpeed);
    }

    /// <summary>
    /// 190813
    /// 플레이어 캐릭터가 보는 방향을 수정
    /// </summary>
    void Update_Player_Dir()
    {
        float original_angle = player.m_BoneHelper.Camera_Head.transform.localRotation.eulerAngles.y;
        player.m_CoreObject.transform.localRotation = Quaternion.Euler(0f, original_angle, 0f);
    }

    /// <summary>
    /// 190812 
    /// 향하려는 방향과 움직이는 정도를 측정한다
    /// </summary>
    public static Vector3 Calculate_Dir(float original_angle, float horizontal, float vertical, float _horizontal_speed, float _vertical_speed)
    {
        /*
        float hor_angle = original_angle + 90f * Mathf.Deg2Rad;
        float vert_angle = original_angle * Mathf.Deg2Rad;

        Vector3 hor_dir = new Vector3(Mathf.Sin(hor_angle), 0f, Mathf.Cos(hor_angle));
        Vector3 vert_dir = new Vector3(Mathf.Sin(vert_angle), 0f, Mathf.Cos(vert_angle));

        hor_dir *= _horizontal_speed;
        vert_dir *= _vertical_speed;

        return hor_dir + vert_dir;
        */

        float dir = Mathf.Atan2(horizontal, vertical) + original_angle * Mathf.Deg2Rad;
        //float speed = 5f * Mathf.Max(Mathf.Max(horizontal, vertical), Mathf.Min(horizontal, vertical) * -1f);

        // Debug.Log("final dir = " + (dir * Mathf.Rad2Deg + original_angle));

        float x = Mathf.Sin(dir);
        float z = Mathf.Cos(dir);

        return new Vector3(x, 0.0f, z);
        // return new Vector3(x * speed, 0.0f, z * speed);
    }

    void Jump()
    {
        player.m_Rigidbody.velocity = Vector3.zero;
        player.m_Rigidbody.AddForce(new Vector3(0f, 6f, 0f), ForceMode.Impulse);
    }

    void GroundEnter()
    {
        player.m_State_Lower = STATE_LOWER.ON_GROUND;
        m_Animator.SetBool("OnGround", true);
    }

    void GroundExit()
    {
        player.m_State_Lower = STATE_LOWER.ON_AIR;
        m_Animator.SetBool("OnGround", false);
    }

    void Crouch(bool _Release)
    {
        if (_Release)
        {
            player.m_State_Lower = STATE_LOWER.ON_GROUND;
        }
        else
        {
            player.m_State_Lower = STATE_LOWER.CROUCH;
        }
        m_Animator.SetBool("OnCrouch", !_Release);
    }
}
