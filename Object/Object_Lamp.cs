﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object_Lamp : MonoBehaviour
{
    [Header("형광등 설정")]
    public GameObject On_Object;
    public GameObject Off_Object;

    public void Toggle_Lamp()
    {
        On_Object.SetActive(!On_Object.activeSelf);
        Off_Object.SetActive(!Off_Object.activeSelf);
    }
}
