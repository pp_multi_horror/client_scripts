﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Malaia.Data;

public class Map : MonoBehaviour
{
    public static Map Instance;

    // Prefab
    public GameObject Player_Prefab;
    public GameObject Phantom_Prefab;
    public GameObject Ghost_Prefab;

    // Parent
    public Transform m_ObjectParent;

    // Var
    public List<Player> m_Players;
    public List<Character_Ghost> m_Ghosts;
    public List<Item> m_Items;

    private void Awake()
    {
        m_Players = new List<Player>();
        m_Ghosts = new List<Character_Ghost>();
        m_Items = new List<Item>();
        Instance = this;
    }

    private void Start()
    {
        // 이벤트 추가
        Ingame_Packet_Handler iph = Ingame_Packet_Handler.Instance;
        iph.m_PlayerSpawn.AddListener(new UnityAction<bool, int, Vector3>(Add_PlayerObject));
        iph.m_GhostSpawn.AddListener(new UnityAction<int, Vector3>(Add_GhostObject));
        

        iph.m_SectorChange.AddListener(new UnityAction<int[]>(Change_Player_State));
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            Add_ItemObject(new Vector3(0f, 2f, 0f), Vector3.zero, 20001, 2001);
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            Add_ItemObject(new Vector3(0f, 2f, 0f), Vector3.zero, 20002, 2002);
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            Add_ItemObject(new Vector3(0f, 2f, 0f), Vector3.zero, 10001, 1001);
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Add_ItemObject(new Vector3(0f, 2f, 0f), Vector3.zero, 10001, 1002);
        }
    }


    /// <summary>
    /// 맵에 플레이어 오브젝트를 생성
    /// </summary>
    /// <param name="_playerdata"></param>
    public void Add_PlayerObject(bool _is_phantom, int _playerNetID, Vector3 _coord)
    {
        Data_Player data = null;

        // 자신이 가진 로비데이터 일람을 살펴봄
        for (int i = 0; i < Data_Lobby.Current_Lobby.m_Players.Count; i++)
        {
            if (Data_Lobby.Current_Lobby.m_Players[i].m_PlayerNetID == _playerNetID)
                data = Data_Lobby.Current_Lobby.m_Players[i];
        }
        // 데이터가 없는 경우
        if (data == null)
        {
            Debug.Log("없는 플레이어인데요?");
            return;
        }

        // 다 옳은 경우
        Window_Ingame_Loading.Instance.Add_String(data.m_Name + " 플레이어를 스폰해요.");

        GameObject player = Instantiate(_is_phantom ? Phantom_Prefab : Player_Prefab, m_ObjectParent);
        player.transform.localScale = new Vector3(1f, 1f, 1f);
        player.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
        player.transform.localPosition = _coord;
        Player pl = player.GetComponent<Player>();
        pl.m_PlayerData = data;
        pl.Set_Character(_is_phantom);
        m_Players.Add(pl);
    }

    /// <summary>
    /// 맵에 고스트 오브젝트를 생성
    /// </summary>
    /// <param name="_playerdata"></param>
    public void Add_GhostObject(int _ghostID, Vector3 _coord)
    {
        GameObject ghost = Instantiate(Ghost_Prefab, m_ObjectParent);
        ghost.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
        ghost.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
        ghost.transform.position = _coord;
        Character_Ghost gh = ghost.GetComponent<Character_Ghost>();
        gh.m_GhostID = _ghostID;
        m_Ghosts.Add(gh);
    }

    /// <summary>
    /// 190906
    /// 맵에 아이템 오브젝트를 생성
    /// 
    /// </summary>
    /// <param name="_coord">아이템 글로벌 포지션 값</param>
    /// <param name="_eulerangle">아이템 글로벌 로테이션 값(오일러각)</param>
    /// <param name="_itemID">아이템의 넷상 ID</param>
    /// <param name="_itemType">아이템이 뭔지 알려주는 ID, 자세한 건 Data_Enum.cs 참고</param>
    public void Add_ItemObject(Vector3 _coord, Vector3 _eulerangle, int _itemID, int _itemType)
    {
        GameObject item_prefab = Data_Item.Get_Item_Prefab((ITEM_INDEX)_itemType);
        if(item_prefab == null)
            throw new System.Exception(_itemType + " 번호의 아이템은 없는뎁쇼");

        GameObject item = Instantiate(item_prefab, m_ObjectParent);
        item.transform.position = _coord;
        item.transform.rotation = Quaternion.Euler(_eulerangle);
        item.transform.localScale = new Vector3(1f, 1f, 1f);
        Item it = item.GetComponent<Item>();
        it.m_Object_ID = _itemID;
        it.m_Index = (ITEM_INDEX)_itemType;

        m_Items.Add(it);
    }

    /// <summary>
    /// 190908
    /// 클라이언트 섹터가 변경되었을 때, On/Off 할 플레이어를 설정
    /// 배열 내에 있는 id들은 Off 되지 않음
    /// </summary>
    /// <param name="_player_netIDs"></param>
    public void Change_Player_State(int[] _player_netIDs)
    {
        List<Data_Player> players = Data_Lobby.Current_Lobby.m_Players;
        for (int i = 0; i < players.Count; i++)
        {
            // 플레이어 스스로의 것은 검출하지 않음
            if (players[i].m_PlayerNetID == Data_Player.GetClientData().m_PlayerNetID)
                continue;

            bool activate = false;
            for (int j = 0; j < _player_netIDs.Length; j++)
            {
                if (players[i].m_PlayerNetID == _player_netIDs[j]) // 비교하려는 플레이어의 ID 가 배열 내에 있는 경우
                {
                    activate = true; // 활성화
                    break;
                }
            }

            Debug.Log(players[i].m_PlayerNetID + "번 활성화 여부 = " + activate);
            Ingame_Packet_Handler.Instance.p_PlayerEnable.Invoke(players[i].m_PlayerNetID, activate);
        }
    }

    
}
