﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Malaia.Data;

public class Interactable_Object_ID_Bank
{
    static Interactable_Object_ID_Bank Instance;
    int m_Used_obj_id = 0;

    public static Interactable_Object_ID_Bank Get_Instance()
    {
        if (Instance == null)
            Instance = new Interactable_Object_ID_Bank();

        return Instance;
    }

    public int Get_OBJ_ID()
    {
        m_Used_obj_id++;
        return m_Used_obj_id;
    }

    public int Get_Item_ID()
    {
        m_Used_obj_id++;
        return m_Used_obj_id;
    }

    public int Get_Equipment_ID()
    {
        m_Used_obj_id++;
        return m_Used_obj_id;
    }
}

/// <summary>
/// 직접적으로 상호작용이 가능한 오브젝트
/// </summary>
public class Interactable_Object : MonoBehaviour
{
    public UnityEvent m_Event_Interact; // 해당 이벤트가 발동하면 오브젝트가 움직이거나 뭐 그럼
    public INTERACT_DESCTYPE m_Interact_Desctype;

    public int m_Final_Act_PlayerID;
    public int m_Object_ID; // 오브젝트 고유값

    public UnityAction<int, int, int, bool> a_interact;

    void Awake()
    {
        Initialize();
    }

    private void Start()
    {
        // 이벤트 등록
        a_interact = new UnityAction<int, int, int, bool>(Interact);
        Ingame_Packet_Handler.Instance.m_Interact.AddListener(a_interact);
    }

    public virtual void Initialize()
    {
        m_Object_ID = Interactable_Object_ID_Bank.Get_Instance().Get_OBJ_ID();
    }

    public virtual void Interact(int _playerNetID, int _objectID, int _state, bool _intantly)
    {
        if (m_Object_ID == _objectID)
        {
            // Debug.Log(m_Object_ID + " == " + _objectID);
            m_Final_Act_PlayerID = _playerNetID;
            m_Event_Interact.Invoke();
        }
    }

    public virtual string Get_Notice_Desc()
    {
        return "\'E\'키를 눌러 " + Data_Interact_Notice.GetDesc(m_Interact_Desctype);
    }

    public void Despawn()
    {
        Ingame_Packet_Handler.Instance.m_Interact.RemoveListener(a_interact);
        Destroy(gameObject);
    }
}
