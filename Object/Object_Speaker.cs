﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object_Speaker : MonoBehaviour
{
    public static List<Object_Speaker> All_Speakers = new List<Object_Speaker>();
    
    public AudioSource m_Audio;
    
    [Header("스피커 설정")]
    public AudioClip[] SchoolBells;

    private void Awake()
    {
        All_Speakers.Add(this);
    }

    public static void Play_SchoolBell(int _index, float _volume)
    {
        foreach(Object_Speaker speaker in All_Speakers)
        {
            speaker.m_Audio.clip = speaker.SchoolBells[_index];
            speaker.m_Audio.volume = _volume;
            speaker.m_Audio.Play();
        }
    }
}
