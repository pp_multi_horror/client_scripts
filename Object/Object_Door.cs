﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Object_Door : Interactable_Object
{
    [Header("문 설정")]
    public float m_Door_MoveRange;
    public int m_Door_AnimFrame;
    [SerializeField]
    bool m_Is_Opened = false;

    Vector3 m_Original_localPos;
    bool m_Is_Playing = false;
    UnityAction func;

    void OnEnable()
    {
        m_Original_localPos = transform.localPosition;
        func = new UnityAction(ToggleDoor);
        m_Event_Interact.AddListener(func);
    }

    void OnDisable()
    {
        m_Event_Interact.RemoveListener(func);
    }

    void ToggleDoor()
    {
        if (m_Is_Playing) // 애니메이션이 진행중인 경우에는 처리 씹음
            return;

        if (m_Is_Opened) StartCoroutine(Close());
        else StartCoroutine(Open());
    }

    IEnumerator Open()
    {
        Debug.Log("door open");
        m_Is_Playing = true;

        for(int current_frame = 0; current_frame < m_Door_AnimFrame; current_frame++)
        {
            float x = m_Original_localPos.x + (m_Door_MoveRange * ((float)current_frame / m_Door_AnimFrame));
            transform.localPosition = new Vector3(x, m_Original_localPos.y, m_Original_localPos.z);
            yield return new WaitForFixedUpdate();
        }

        m_Is_Opened = true;
        m_Is_Playing = false;
        yield return null;
    }

    IEnumerator Close()
    {
        Debug.Log("door close");
        m_Is_Playing = true;
        
        for (int current_frame = m_Door_AnimFrame; current_frame > 0; current_frame--)
        {
            float x = m_Original_localPos.x + (m_Door_MoveRange * ((float)current_frame / m_Door_AnimFrame));
            transform.localPosition = new Vector3(x, m_Original_localPos.y, m_Original_localPos.z);
            yield return new WaitForFixedUpdate();
        }

        m_Is_Opened = false;
        m_Is_Playing = false;
        yield return null;
    }
}
