﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Malaia.Data;

public class MainCam : MonoBehaviour
{
    public static MainCam Instance;
    public GameObject m_DoF_Axis;
    public Light m_CamcoderLight;

    float m_Normal_FOV = 60f;
    float m_Focused_FOV = 15f;

    bool m_Locked = false;
    Camera m_Cam;
    Player m_SeeingPlayer;
    Ingame_Packet_Handler IPH;
    Ingame_GUI_Manager IGM;

    private void Awake()
    {
        Instance = this;
        m_Cam = GetComponentInChildren<Camera>();
    }

    IEnumerator Start()
    {
        while (Ingame_Packet_Handler.Instance == null)
            yield return new WaitForEndOfFrame();
        while (Ingame_GUI_Manager.Instance == null)
            yield return new WaitForEndOfFrame();

        IPH = Ingame_Packet_Handler.Instance;
        IGM = Ingame_GUI_Manager.Instance;
    }

    /// <summary>
    /// 카메라 시점을 전환한다
    /// </summary>
    /// <param name="_player"></param>
    public void Change_Camera_Parent(Player _player)
    {
        m_SeeingPlayer = _player;
        transform.SetParent(_player.m_BoneHelper.Camera_Head);
        yaw = 0f; pitch = 0f;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
    }

    float yaw = 0f;
    float pitch = 0f;

    private void Update()
    {
        if (m_Locked) // 마우스로 시선을 옮길 수 있는 경우
        {
            if (m_SeeingPlayer != null) // 플레이어를 보고있는 중인 경우
            {
                if (m_SeeingPlayer.m_PlayerData == Data_Player.GetClientData()) // 플레이어가 클라이언트 자신인 경우
                {
                    bool focused = Input.GetKey(KeyCode.Space);
                    if (!Input.GetAxis("Mouse X").Equals(0f) || !Input.GetAxis("Mouse Y").Equals(0f))
                    {
                        float mult = focused ? 0.4f : 2f;
                        yaw += Input.GetAxis("Mouse X") * mult;
                        pitch += Input.GetAxis("Mouse Y") * mult;
                        if (pitch > 89f || pitch < -89f)
                            pitch = Mathf.Min(89f, Mathf.Max(-89f, pitch));

                        IPH.p_Look.Invoke(m_SeeingPlayer.m_PlayerData.m_PlayerNetID, pitch, yaw);
                    }
                    m_Cam.fieldOfView = Mathf.Lerp(m_Cam.fieldOfView, (focused ? m_Focused_FOV : m_Normal_FOV), 0.15f);
                }
            }
        }

        // 캠코더 빛
        m_CamcoderLight.gameObject.SetActive(IGM.m_Cam_HUD.activeSelf);

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out RaycastHit hit, 200f); // 이 때 맞는 건 Trigger 체크 된 콜라이더
        m_DoF_Axis.transform.position = hit.point;

        // 디버그
        if (Input.GetKeyDown(KeyCode.Escape))
            Lock_Cursor(false);
        if (Input.GetMouseButtonDown(0))
            Lock_Cursor(true);
    }

    /// <summary>
    /// 마우스 커서를 잠근다
    /// true로 해야 잠김
    /// </summary>
    public void Lock_Cursor(bool _state)
    {
        Cursor.lockState = _state ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = !_state;
        m_Locked = _state;
    }
}
