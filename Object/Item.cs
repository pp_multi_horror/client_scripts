﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Malaia.Data;

/*
 * 모든 아이템의 베이스
 * 오브젝트 ID 시작점은 100000 부터 하는 것으로
 */
public class Item : Interactable_Object
{
    public ITEM_INDEX m_Index;

    void Awake()
    {
    }

    public override void Initialize()
    {
    }

    // 이건 습득이겠지
    public override void Interact(int _playerNetID, int _objectID, int _state, bool _intantly)
    {
        if (m_Object_ID == _objectID)
        {
            // Debug.Log(m_Object_ID + " == " + _objectID);
            m_Final_Act_PlayerID = _playerNetID;

            foreach(Data_Player data in Data_Lobby.Current_Lobby.m_Players)
            {
                if(data.m_PlayerNetID == _playerNetID)
                {
                    // 1000번대 = 장비
                    if(m_Index < ITEM_INDEX.CONSUME_START)
                    {
                        data.m_Ingame_Equipment = m_Index;
                    }

                    // 2000번대 = 소모품
                    else
                    {
                        if (data.m_Ingame_Items[0] == ITEM_INDEX.NONE)
                            data.m_Ingame_Items[0] = m_Index;
                        else if (data.m_Ingame_Items[1] == ITEM_INDEX.NONE)
                            data.m_Ingame_Items[1] = m_Index;
                        else
                            throw new System.Exception("Player Item is Full - Item : 46 line");
                    }

                    if (data == Data_Player.GetClientData())
                        Ingame_GUI_Manager.Instance.Update_Toolbar();
                }
            }

            Map.Instance.m_Items.Remove(this);
            Despawn();
        }
    }

    public override string Get_Notice_Desc()
    {
        return "\'E\'키를 눌러 " + Data_Interact_Notice.GetDesc_Item(m_Index) + " 획득";
    }
}
