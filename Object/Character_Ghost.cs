﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Malaia.Data;

public class Character_Ghost : MonoBehaviour
{
    public SpriteRenderer m_Ghost;
    public AudioSource m_AudioSource;
    public AudioClip[] m_Sounds;
    public AudioClip m_HitSound;

    public int m_GhostID;

    Camera main_cam;
    Coroutine m_Coroutine_Move;
    Vector3 m_Target_Coord;
    Vector3 m_Dir;
    float m_Remained_Time;
    float m_Remained_Time_Full;

    UnityAction<int, Vector3, float> a_move = null;
    UnityAction<int, int> a_hit = null;
    UnityAction<int> a_despawn = null;

    private void Awake()
    {
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        main_cam = Camera.main;
        StartCoroutine(Play_Sound());

        while (Ingame_Manager.Instance == null)
            yield return new WaitForEndOfFrame();

        a_move = new UnityAction<int, Vector3, float>(Move_Ghost);
        a_hit = new UnityAction<int, int>(Hit_Human);
        a_despawn = new UnityAction<int>(Despawn);
        Ingame_Packet_Handler.Instance.e_GhostMove.AddListener(a_move);
        Ingame_Packet_Handler.Instance.e_GhostHit.AddListener(a_hit);
        Ingame_Packet_Handler.Instance.m_GhostDeSpawn.AddListener(a_despawn);
        //Ingame_Manager.Instance.m_Ghost_Move_Event.AddListener(new UnityAction<int, Vector3, float>(Move_Ghost));
        //Ingame_Manager.Instance.m_Ghost_Hit_Event.AddListener(new UnityAction<int, int>(Hit_Human));
    }

    // Update is called once per frame
    void Update()
    {
        m_Ghost.transform.rotation = Quaternion.Euler(0f, main_cam.transform.rotation.eulerAngles.y + 180f, 0f);
    }

    // 주변에 있으면 들리는 옹알이
    IEnumerator Play_Sound()
    {
        float distance = 15f;

        while(gameObject != null)
        {
            float comp_dist = Vector3.Distance(main_cam.transform.position, transform.position);

            // 메인 카메라와의 거리가 15 이하
            if (comp_dist < distance)
            {
                // 메인 카메라와의 높이차가 -1.0~1.8
                if (main_cam.transform.position.y - transform.position.y < 1.8f || main_cam.transform.position.y - transform.position.y > -1f)
                {
                    m_AudioSource.clip = m_Sounds[Random.Range(0, m_Sounds.Length - 1)];
                    m_AudioSource.volume = comp_dist / distance;
                    m_AudioSource.Play();

                    // 플레이어가 귀신이 아니어야 함
                    if (!Data_Player.GetClientData().m_Ingame_Is_Phantom)
                    { }
                }
            }
            yield return new WaitForSeconds(3f);
        }
        yield return null;
    }

    /// <summary>
    /// 어느 좌표를 향해 이동
    /// 속도가 따로 없고, 시간에 맞춰 움직이게 해둠
    /// </summary>
    /// <param name="_ghostID">움직이려는 고스트의 ID</param>
    /// <param name="_coord">향하는 좌표(를 향해 직선으로 움직인다)</param>
    /// <param name="_time">거기까지 가는데 걸리는 시간, 0 이하인 경우 해당 좌표로 순간이동</param>
    public void Move_Ghost(int _ghostID, Vector3 _coord, float _time)
    {
        if(m_GhostID == _ghostID)
        {
            if (m_Coroutine_Move != null)
            {
                StopCoroutine(m_Coroutine_Move);
                transform.position = m_Target_Coord;
            }

            m_Remained_Time_Full = _time;
            m_Remained_Time = _time;
            m_Target_Coord = _coord;
            m_Dir = m_Target_Coord - transform.position;

            m_Coroutine_Move = StartCoroutine(Move());
        }
    }

    IEnumerator Move()
    {
        while (m_Remained_Time > 0f)
        {
            m_Remained_Time -= Time.deltaTime;
            transform.position = m_Target_Coord - m_Dir * (m_Remained_Time / m_Remained_Time_Full);

            yield return new WaitForEndOfFrame();
        }
        transform.position = m_Target_Coord;
        yield return null;
    }

    /// <summary>
    /// 사람을 쳤을 때
    /// </summary>
    /// <param name="_playerNetID"></param>
    /// <param name="_ghostID"></param>
    public void Hit_Human(int _playerNetID, int _ghostID)
    {
        if(_playerNetID == Data_Player.GetClientData().m_PlayerNetID)
        {
            StartCoroutine(Ingame_GUI_Manager.Instance.JumpScare());
        }
        m_AudioSource.clip = m_HitSound;
        m_AudioSource.Play();
    }

    public void Despawn(int _ghostID)
    {
        Map.Instance.m_Ghosts.Remove(this);

        Ingame_Packet_Handler.Instance.e_GhostMove.RemoveListener(a_move);
        Ingame_Packet_Handler.Instance.e_GhostHit.RemoveListener(a_hit);
        Ingame_Packet_Handler.Instance.m_GhostDeSpawn.RemoveListener(a_despawn);

        Destroy(gameObject);
    }
}
