﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Window_Title : Window_Base
{
    public Window_Title()
    {
        m_Menus = new string[3] { "JOIN", "REPLAY", "EXIT" };
    }

    public override void Update_Input()
    {
        base.Update_Input();
        if (Input.GetKeyDown(KeyCode.Return))
        {
            TM.Play_OKSound();
            switch (m_Cursor)
            {
                case 0: // 서버 들가기
                    TM.m_State = Title_Manager.MENU_STATE.JOIN;
                    TM.Refresh_Window();
                    break;
                case 1: // 리플레이
                    break;
                case 2: // 겜 종료
                    Application.Quit();
                    break;
            }
        }
    }
}
