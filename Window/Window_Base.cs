﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Window_Base : MonoBehaviour
{
    protected Title_Manager TM;

    public Text m_Desc;
    protected string[] m_Menus;
    protected int m_Cursor;

    IEnumerator Start()
    {
        while (Title_Manager.Instance == null)
            yield return new WaitForEndOfFrame();

        TM = Title_Manager.Instance;
        yield return null;
    }

    public virtual void Initialize()
    { }

    public virtual void Update_Input()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            TM.Play_ClickSound();
            m_Cursor = m_Cursor + 1 > m_Menus.Length - 1 ? 0 : m_Cursor + 1;
            Update_Text();
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            TM.Play_ClickSound();
            m_Cursor = m_Cursor - 1 < 0 ? m_Menus.Length - 1 : m_Cursor - 1;
            Update_Text();
        }
    }

    public virtual void Update_Text()
    {
        if (m_Desc == null) // 설명 문구 설정 안했으면 그냥 스킵
            return;

        m_Desc.text = "";
        for(int i = 0; i < m_Menus.Length; i++)
            m_Desc.text += m_Menus[i] + (m_Cursor == i ? "<" : " ") + (i != m_Menus.Length - 1 ? "\n" : "");
    }
}
