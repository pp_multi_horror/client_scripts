﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Element_Supply : MonoBehaviour
{
    public bool is_equipment; // 장비창?

    public Image[] m_Icons; // 아이콘 목록
    Image m_Current_Active_Icon;

    public void Update_Supply_Image(int _index)
    {
        if(m_Current_Active_Icon != null)
            m_Current_Active_Icon.gameObject.SetActive(false);

        if(_index >= 0)
        {
            m_Icons[_index].gameObject.SetActive(true);
            m_Current_Active_Icon = m_Icons[_index];
        }
    }
}
