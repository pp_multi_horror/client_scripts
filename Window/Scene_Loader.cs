﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// 로딩할 때 쓰이는 오브젝트
/// </summary>
public class Scene_Loader : MonoBehaviour
{
    public Text m_Loading_Text;
    public UnityEvent m_Load_Complete;

    public void Awake()
    {
        m_Load_Complete = new UnityEvent();
        DontDestroyOnLoad(gameObject);
    }

    public void To_Ingame()
    {
        StartCoroutine(LoadScene("Ingame", true));
    }

    public void To_Lobby()
    {
        StartCoroutine(LoadScene("Title", false));
    }

    public IEnumerator LoadScene(string _scene_name, bool _wait)
    {
        AsyncOperation ao = SceneManager.LoadSceneAsync(_scene_name);
        while(!ao.isDone)
        {
            m_Loading_Text.text = "LOADING\n" + (int)(ao.progress * 100) + "%";
            yield return new WaitForSeconds(0.1f);
        }
        m_Load_Complete.Invoke();
        if(_wait)
        {
            m_Loading_Text.text = "PRESS ANY KEY TO START";
            while (!Input.anyKey)
                yield return new WaitForEndOfFrame();
        }
        Destroy(gameObject);
        yield return null;
    }
}
