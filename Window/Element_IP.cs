﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IP_Data
{
    public string IP;
    public string Port;
    public string ServerName;
    public string NickName;

    public IP_Data(string _ip, string _port, string _server_name, string _nickname)
    {
        IP = _ip;
        Port = _port;
        ServerName = _server_name;
        NickName = _nickname;
    }

    public static void Save_Data(int _index, IP_Data _data)
    {
        PlayerPrefs.SetString("Server.IP_Cache_" + _index + "_IP", _data.IP);
        PlayerPrefs.SetString("Server.IP_Cache_" + _index + "_Port", _data.Port);
        PlayerPrefs.SetString("Server.IP_Cache_" + _index + "_ServerName", _data.ServerName);
        PlayerPrefs.SetString("Server.IP_Cache_" + _index + "_NickName", _data.NickName);
    }

    public static IP_Data Load_Data(int _index)
    {
        string ip = "", port = "", server_name = "", nickname = "";

        ip = PlayerPrefs.GetString("Server.IP_Cache_" + _index + "_IP", "");
        port = PlayerPrefs.GetString("Server.IP_Cache_" + _index + "_Port", "");
        server_name = PlayerPrefs.GetString("Server.IP_Cache_" + _index + "_ServerName", "");
        nickname = PlayerPrefs.GetString("Server.IP_Cache_" + _index + "_NickName", "");

        if (ip.Equals("") || port.Equals(""))
            return null;

        return new IP_Data(ip, port, server_name, nickname);
    }

    public static void Delete_Data(int _index)
    {
        PlayerPrefs.DeleteKey("Server.IP_Cache_" + _index + "_IP");
        PlayerPrefs.DeleteKey("Server.IP_Cache_" + _index + "_Port");
        PlayerPrefs.DeleteKey("Server.IP_Cache_" + _index + "_ServerName");
        PlayerPrefs.DeleteKey("Server.IP_Cache_" + _index + "_NickName");
    }
}

public class Element_IP : MonoBehaviour
{
    public Image BackImage; // 뒷배경
    public Text IP_Nick_Text;
    public Text Connectable_Text;
    public Text Cursor_Text;

    public IP_Data m_Data;
    public int m_Index; // 현재 커서 인덱스, 0은 연결 1은 제거
    bool m_Focused; // 현재 포커스 된 상태?

    public void Init(IP_Data _data)
    {
        m_Data = _data;
        RectTransform rt = GetComponent<RectTransform>();
        rt.localPosition = Vector3.zero;
        rt.localScale = new Vector3(1f, 1f, 1f);

        if (m_Data.ServerName.Equals(""))
            IP_Nick_Text.text = Get_ServerName() + "\n" + m_Data.NickName;
        else
            IP_Nick_Text.text = Get_ServerName() + "\n" + m_Data.NickName;
        Connectable_Text.text = "\n" + m_Data.IP + ":" + m_Data.Port;

        Update_Color();
        Update_Text();
    }

    public string Get_ServerName()
    {
        string text = "";
        if (m_Data.ServerName.Equals(""))
            text = m_Data.IP + ":" + m_Data.Port;
        else
            text = m_Data.ServerName;
        return text;
    }

    /// <summary>
    /// 포커스 변경
    /// </summary>
    public void Toggle_Focus()
    {
        m_Focused = !m_Focused;
        Update_Color();
        Update_Text();
    }

    /// <summary>
    /// 커서 인덱스 변경
    /// 0은 연결, 1은 제거
    /// </summary>
    public void Toggle_Index()
    {
        m_Index = m_Index == 1 ? 0 : 1;
        Update_Text();
    }

    public void Update_Text()
    {
        if (m_Focused)
        {
            Cursor_Text.text = m_Index == 0 ? "[CONNECT]" : " CONNECT ";
            Cursor_Text.text += "      ";
            Cursor_Text.text += m_Index == 1 ? "[REMOVE]" : " REMOVE ";
        }
        else
        {
            Cursor_Text.text = " CONNECT ";
            Cursor_Text.text += "      ";
            Cursor_Text.text += " REMOVE ";
        }
    }

    void Update_Color()
    {
        Color white = new Color(1f, 1f, 1f, 0.57f);
        BackImage.color = m_Focused ? white : Color.black;
        IP_Nick_Text.color = m_Focused ? Color.black : white;
        Connectable_Text.color = m_Focused ? Color.black : white;
        Cursor_Text.color = m_Focused ? Color.black : white;
    }
}
