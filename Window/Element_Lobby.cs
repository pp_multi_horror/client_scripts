﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Malaia.Data;

public class Element_Lobby : MonoBehaviour
{
    // Object
    public Image BackImage; // 뒷배경
    public Text LobbyText; // 로비의 이름과 참여인원
    public Text StateText; // 현재 로비 상태 텍스트

    // Data
    public Data_Lobby m_Data; // 소유중인 로비 데이터값
    bool m_Focused; // 현재 포커스 된 상태?
    
    public void Init(Data_Lobby _data)
    {
        m_Data = _data;
        RectTransform rt = GetComponent<RectTransform>();
        rt.localPosition = Vector3.zero;
        rt.localScale = new Vector3(1f, 1f, 1f);
        
        LobbyText.text = m_Data.m_Lobby_Name + "\n" + m_Data.m_Current_User + " / 5";
        StateText.text = m_Data.m_Now_Playing ? "PLAYING" : (m_Data.m_Current_User >= 5 ? "FULL" : "WAITING");

        Update_Color();
    }

    /// <summary>
    /// 포커스 여부를 판단
    /// </summary>
    public void Toggle_Focus()
    {
        m_Focused = !m_Focused;
        Update_Color();
    }

    void Update_Color()
    {
        Color white = new Color(1f, 1f, 1f, 0.57f);
        BackImage.color = m_Focused ? white : Color.black;
        LobbyText.color = m_Focused ? Color.black : white;
        StateText.color = m_Focused ? Color.black : white;
    }
}
