﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Malaia.Data;

public class Window_Ingame_Loading : MonoBehaviour
{
    public static Window_Ingame_Loading Instance;

    public Text LoadingText;
    public Text DebugText;

    string debug_str;

    Ingame_Manager IM;
    Ingame_Input_Manager IIM;

    Ingame_Packet_Handler IPH;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        if (Ingame_Manager.Instance == null)
            yield return new WaitForEndOfFrame();
        if (Ingame_Input_Manager.Instance == null)
            yield return new WaitForEndOfFrame();
        if (Ingame_Packet_Handler.Instance == null)
            yield return new WaitForEndOfFrame();
        IM = Ingame_Manager.Instance;
        IIM = Ingame_Input_Manager.Instance;
        IPH = Ingame_Packet_Handler.Instance;

        Add_DebugListener();
    }

    // Update is called once per frame
    void Update()
    {
        string players = "";
        foreach(Data_Player pl in Data_Lobby.Current_Lobby.m_Players)
        {
            players += pl.m_Name + (pl.m_Ingame_Loading_Complete ? " - Ready" : " - Not yet");
            players += "\n";
        }

        LoadingText.text =
            "자신의 로딩 상태\n" +
            (Data_Player.GetClientData().m_Ingame_Loading_Complete ? "OK" : "Not yet") +
            "\n\n현재 핑 (마지막 핑 / 핑 평균 / 오간 횟수)\n" +
            IM.m_Ping.Get_PingData() +
            "\n\n로딩 완료된 플레이어\n" +
            players;
    }

    /// <summary>
    /// 190830
    /// 디버그 리스너 추가 함수
    /// </summary>
    void Add_DebugListener()
    {
        // 플레이어 섹터 진입 이벤트
        IPH.p_PlayerEnable.AddListener(new UnityAction<int, bool>((int _netID, bool _activate) =>
        {
            Add_String(Get_PlayerName(_netID) + " - " + (_activate ? "활성화" : "비활성화") + " 되었어요.");
        }));

        IPH.p_Move.AddListener(new UnityAction<int, Vector3, Vector2>((int _netID, Vector3 _coord, Vector2 _dir) =>
        {
            if (_netID == Data_Player.GetClientData().m_PlayerNetID)
                return;
            Add_String(Get_PlayerName(_netID) + " - " + _coord.ToString() + " 으로부터 " + _dir.ToString() + " 방향으로 가고 었어요.");
        }));

        IPH.p_Look.AddListener(new UnityAction<int, float, float>((int _netID, float _pitch, float _yaw) =>
        {
            if (_netID == Data_Player.GetClientData().m_PlayerNetID)
                return;
            Add_String(Get_PlayerName(_netID) + " - 위아래각도: " + _pitch + " 양옆각도: " + _yaw + " 를 보고 었어요.");
        }));

        IPH.p_Crouch.AddListener(new UnityAction<int, bool>((int _netID, bool _activate) =>
        {
            Add_String(Get_PlayerName(_netID) + " - " + (_activate ? "숙였어요." : "섰어요."));
        }));

        IPH.p_Dash.AddListener(new UnityAction<int, bool>((int _netID, bool _activate) =>
        {
            Add_String(Get_PlayerName(_netID) + " - " + (_activate ? "달려요." : "걸어요."));
        }));

        IPH.m_Interact.AddListener(new UnityAction<int, int, int, bool>((int _netID, int _objectID, int _state, bool _intantly) =>
        {
            Add_String(Get_PlayerName(_netID) + " - " + _objectID + "번 오브젝트를 " + _state + " 상태로 만들었어요. " + (_intantly ? "즉시!" : "천천히~"));
        }));

        IPH.p_UseItem.AddListener(new UnityAction<int, int, bool>((int _netID, int _slot, bool _activate) =>
        {
            Add_String(Get_PlayerName(_netID) + " - " + _slot + "번 아이템을 " + (_activate ? "활성화" : "비활성화") + "했어요.");
        }));

        IPH.m_GhostSpawn.AddListener(new UnityAction<int, Vector3>((int _ghostID, Vector3 _coord) =>
        {
            Add_String(_ghostID + "번 고스트 - " + _coord + " 지점에 소환되었어요.");
        }));

        IPH.m_GhostDeSpawn.AddListener(new UnityAction<int>((int _ghostID) =>
        {
            Add_String(_ghostID + "번 고스트 - 사라졌어요.");
        }));

        IPH.e_GhostMove.AddListener(new UnityAction<int, Vector3, float>((int _ghostID, Vector3 _coord, float _time) =>
        {
            Add_String(_ghostID + "번 고스트 - " + _coord + "를 향해서 " + _time + " 초 동안 가요.");
        }));

        IPH.e_GhostHit.AddListener(new UnityAction<int, int>((int _netID, int _ghostID) =>
        {
            Add_String(_ghostID + "번 고스트 - " + Get_PlayerName(_netID) + "와 충돌!!");
        }));
    }

    string Get_PlayerName(int _netID)
    {
        foreach(Data_Player data in Data_Lobby.Current_Lobby.m_Players)
        {
            if (data.m_PlayerNetID == _netID)
            {
                if (data == Data_Player.GetClientData())
                    return "너님" + "(" + _netID + ")";
                else
                    return data.m_Name + "(" + _netID + ")";
            }
        }
        return _netID + "번 플레이어";
    }

    public void Add_String(string _str)
    {
        debug_str += _str;
        debug_str += "\n";

        if(debug_str.Length > 1000)
        {
            debug_str.Substring(0, debug_str.Length - 1000);
        }

        DebugText.text = debug_str;
    }
}
