﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Malaia.Data;

public class Window_Join : Window_Base
{
    //public SocketConnect socket;
    public NetworkManager net;

    public InputField m_IP;             // 접속할 IP
    public InputField m_Port;         // 포트
    public InputField m_ServerNameField;       // 서버 이름
    public InputField m_NameField;       // 자기가 쓸 이름

    public Text m_ErrorText;        // 에러 구문

    bool m_Busy; // 다른 일 하는 중
    Coroutine m_LoginProcess;

    public GameObject Prefab_IP_Element;
    public RectTransform IP_Axis;
    List<Element_IP> m_IP_List;     // IP 요소 리스트
    Element_IP m_Current_Focus_IP;
    int m_List_Cursor;                   // 서버 리스트 커서 값
    bool m_Is_Focus_List;              // 서버 리스트로 포커스가 옮겨갔는지 여부

    public Window_Join()
    {
        m_Menus = new string[6] { "IP", "PORT", "SERVER NAME", "NICKNAME", "CREATE", "EXIT" };
        m_IP_List = new List<Element_IP>();
    }

    // Title_Manager가 이 윈도우를 부르면서 Initialize 한다
    public override void Initialize()
    {
        // 변수 초기화
        m_ErrorText.text = "";

        // 서버 리스트 불러오기
        Refresh_IP_List();
    }

    public override void Update_Input()
    {
        if (m_Busy) return; // 네트워크 접속 시도 중인 경우 입력 차단


        if (m_Is_Focus_List) // 현재 포커스가 로비 리스트를 가리키는 경우
        {
            if (Input.GetKeyDown(KeyCode.DownArrow)) // 리스트 커서 변경
            {
                TM.Play_ClickSound();
                m_List_Cursor = m_List_Cursor + 1 > m_IP_List.Count - 1 ? 0 : m_List_Cursor + 1;
                Change_Focus(m_IP_List[m_List_Cursor]);
                Change_List_Axis();
            }
            if (Input.GetKeyDown(KeyCode.UpArrow)) // 리스트 커서 변경
            {
                TM.Play_ClickSound();
                m_List_Cursor = m_List_Cursor - 1 < 0 ? m_IP_List.Count - 1 : m_List_Cursor - 1;
                Change_Focus(m_IP_List[m_List_Cursor]);
                Change_List_Axis();
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow)) // 연결 <-> 제거 <-> 메뉴 오가기
            {
                TM.Play_ClickSound();
                if (m_Current_Focus_IP.m_Index == 1) // 제거
                    m_Current_Focus_IP.Toggle_Index();
            }
            if (Input.GetKeyDown(KeyCode.RightArrow)) // 연결 <-> 제거 <-> 메뉴 오가기
            {
                TM.Play_ClickSound();
                if (m_Current_Focus_IP.m_Index == 1) // 제거
                {
                    m_Is_Focus_List = false;
                    Change_Focus(null);
                }
                else if (m_Current_Focus_IP.m_Index == 0) // 연결
                    m_Current_Focus_IP.Toggle_Index();
            }
            if (Input.GetKeyDown(KeyCode.Return)) // 현재 선택한 로비로 접속 시도
            {
                TM.Play_OKSound();
                if (m_Current_Focus_IP.m_Index == 1) // 제거
                {
                    m_ErrorText.text = m_Current_Focus_IP.Get_ServerName() + " REMOVED";
                    m_IP_List.Remove(m_Current_Focus_IP);
                    Destroy(m_Current_Focus_IP.gameObject);
                    Save_IP_List();
                    Refresh_IP_List();
                    if (m_IP_List.Count == 0)
                    {
                        m_Is_Focus_List = false;
                        Change_Focus(null);
                    }
                }
                else // 연결
                    m_LoginProcess = StartCoroutine(Login());
            }
        }
        else // 포커스가 메뉴를 가리키는 경우
        {
            base.Update_Input();
            switch (m_Cursor)
            {
                case 0: // IP
                    m_IP.Select();
                    break;
                case 1: // PORT
                    m_Port.Select();
                    break;
                case 2: // ServerNAME
                    m_ServerNameField.Select();
                    break;
                case 3: // NickNAME
                    m_NameField.Select();
                    break;
                default:
                    EventSystem.current.SetSelectedGameObject(null);
                    break;
            }
            if (Input.GetKeyDown(KeyCode.Return))
            {
                TM.Play_OKSound();
                switch (m_Cursor)
                {
                    case 4: // 접속 경로 만들기
                        if (m_IP_List.Count >= 10)
                        {
                            // 리스트가 10개 이상이면 불가능처리
                            m_ErrorText.text = "너무 많은 접속 경로";
                            return;
                        }
                        if (m_IP.text.Equals("") || m_Port.Equals("") || m_NameField.text.Equals(""))
                        {
                            // IP, Port, NameField가 빈칸이면 불가능처리
                            m_ErrorText.text = "입력 구간 누락";
                            return;
                        }
                        Add_IP(new IP_Data(m_IP.text, m_Port.text, m_ServerNameField.text, m_NameField.text));
                        Save_IP_List();
                        break;
                    case 5: // 타이틀로
                        TM.m_State = Title_Manager.MENU_STATE.MAIN;
                        TM.Refresh_Window();
                        break;
                }
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow)) // 리스트 <-> 메뉴 오가기
            {
                TM.Play_ClickSound();
                if (m_IP_List.Count > 0) // 로비 오브젝트 리스트에 뭐가 하나라도 있어야지만 리스트로 커서 변경이 가능
                {
                    m_Is_Focus_List = true;
                    Change_Focus(m_IP_List[m_List_Cursor]);
                }
            }
        }
    }

    public override void Update_Text()
    {
        if (m_Desc == null) // 설명 문구 설정 안했으면 그냥 스킵
            return;

        m_Desc.text = "";
        for (int i = 0; i < m_Menus.Length; i++)
            m_Desc.text += m_Menus[i] + (m_Cursor == i ? "<" : " ") + (i != m_Menus.Length - 1 ? "\n\n" : "");
    }

    /// <summary>
    /// 로그인 프로세스
    /// </summary>
    IEnumerator Login()
    {
        m_Busy = true; // 입력을 차단한다
        bool error = false;

        IP_Data data = m_Current_Focus_IP.m_Data;
        string ip = data.IP;
        string port = data.Port;
        string nickname = data.NickName;

        net.SeverConnet(ip, port); // 서버 연결

        net.Login(nickname); // 서버 로그인 시도

        if (!error)
        {
            Data_Player.GetClientData().m_Name = nickname;
            m_ErrorText.text = "CONNECTING......";

            for (int i = 0; i < 3; i++)
            {
                // TODO 서버에게 1초 단위로 접속을 3번 시도하기
                yield return new WaitForSecondsRealtime(1f);
            }
            // 임시 - 접속 체크가 되어서 Getted_Login_Success 메소드가 작동되면 이건 지워야 함

            TM.m_State = Title_Manager.MENU_STATE.LOBBYLIST;
            TM.Refresh_Window();
            // 임시 끝

            // 여기까지 해도 안되면 대기 시간 초과로 판정, 아래 문구 출력하고 코루틴 끝내기
            ConnectError("서버 응답 없음");
        }
        m_Busy = false;

        yield return null;
    }

    // TODO Getted_Login_Success
    /// <summary>
    /// 서버에게서 우리 서버에 어서와 패킷을 받음
    /// 이 때 반드시 NetID를 발급받아야 함
    /// ......여기서 조금 고민인게 서버에게서 플레이어 데이터를 통째로 받을지
    /// 넷 아이디만 받고 플레이어 데이터는 그냥 일정 시점에서 초기화해줄지 뭐 그런 걱정
    /// </summary>
    public void Getted_Login_Success(int _netID)
    {
        // 접속 코루틴을 멈춤
        StopCoroutine(m_LoginProcess);
        m_Busy = false;
        m_ErrorText.text = "";

        // 로비 리스트 화면으로 넘어감
        Data_Player.GetClientData().m_PlayerNetID = _netID;
        TM.m_State = Title_Manager.MENU_STATE.LOBBYLIST;
        TM.Refresh_Window();
    }

    /// <summary>
    /// 서버에게서 우리 서버에서 꺼져 패킷을 받음
    /// 뭐 이런저런 이유로
    /// </summary>
    public void Getted_Login_Failed(string _error)
    {
        // 접속 코루틴을 멈춤
        StopCoroutine(m_LoginProcess);
        m_Busy = false;
        m_ErrorText.text = "CONNECTION ERROR\n" + _error;
    }

    void ConnectError(string _error)
    {
        m_ErrorText.text = "CONNECTION ERROR\n" + _error;
    }

    /// <summary>
    /// IP 리스트 초기화
    /// </summary>
    public void Refresh_IP_List()
    {
        for (int i = 0; i < m_IP_List.Count; i++)
        {
            Destroy(m_IP_List[i].gameObject);
        }

        m_List_Cursor = 0; // 리스트 커서를 0으로
        m_IP_List = new List<Element_IP>();
        for (int i = 0; i < 10; i++)
        {
            IP_Data data = IP_Data.Load_Data(i);

            if (data == null) // 데이터가 없으면 버리기
                break;

            Add_IP(data);
        }
    }

    /// <summary>
    /// IP 리스트 저장
    /// </summary>
    public void Save_IP_List()
    {
        for (int i = 0; i < 10; i++)
        {
            if (m_IP_List.Count <= i)
                IP_Data.Delete_Data(i);
            else
                IP_Data.Save_Data(i, m_IP_List[i].m_Data);
        }
    }

    /// <summary>
    /// 리스트에 IP 요소 추가
    /// </summary>
    public void Add_IP(IP_Data _data)
    {
        Element_IP ip = Instantiate(Prefab_IP_Element, IP_Axis).GetComponent<Element_IP>();
        ip.Init(_data);
        ip.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -240f * m_IP_List.Count);
        m_IP_List.Add(ip);
    }

    /// <summary>
    /// 로비 오브젝트 선택을 변경
    /// </summary>
    /// <param name="_lobby"></param>
    public void Change_Focus(Element_IP _ip)
    {
        if (m_Current_Focus_IP != null)
            m_Current_Focus_IP.Toggle_Focus();
        m_Current_Focus_IP = _ip;
        if (m_Current_Focus_IP != null)
            m_Current_Focus_IP.Toggle_Focus();
    }

    /// <summary>
    /// 로비 리스트 커서가 바꼈을 때 리스트의 축을 변경
    /// </summary>
    public void Change_List_Axis()
    {
        if (m_IP_List.Count > 2 && m_List_Cursor > 2)
        {
            IP_Axis.anchoredPosition = new Vector2(0f, 240f * (m_List_Cursor - 2));
        }
        else
        {
            IP_Axis.anchoredPosition = Vector2.zero;
        }
    }
}
