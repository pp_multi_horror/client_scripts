﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Malaia.Data;
using System.Threading;

public class Window_Lobby : Window_Base
{
    [Header("플레이어")]
    List<Data_Player> m_Login_Players;     // 서버에 있는 사람들의 닉네임
    public Text m_PlayerList;                   // 플레이어 리스트 텍스트

    [Header("채팅")]
    public InputField m_Chat;                   // 자신의 채팅 Field
    public RectTransform m_Chat_Mask;   // 채팅 윈도우의 마스크 오브젝트
    public GameObject Chat_Prefab;         // 채팅 문구 프리팹
    List<Element_Chat> m_Chats;             // 현재 오브젝트에 올라와 있는 문구들

    public GameObject Scene_Loader_Prefab;

    public NetworkManager net;
    PacketManager packet;

    public Window_Lobby()
    {
        m_Menus = new string[3] { "CHAT", "READY", "EXIT" };
        m_Chats = new List<Element_Chat>();
    }
    
    // Title_Manager가 이 윈도우를 부르면서 Initialize 한다
    public override void Initialize()
    {
        if(m_Chats.Count > 0)
        foreach (Element_Chat chat in m_Chats)
            Destroy(chat.gameObject);
        m_Chats = new List<Element_Chat>();

        // 임시 - 플레이어 리스트 출력 테스트
        m_Login_Players = new List<Data_Player>();
        m_Login_Players.Add(Data_Player.GetClientData());
//        Refresh_PlayerList();
        // 임시끝
        
        // TODO 서버에게 로비에 누구누구 있나 알려달라고 하기
        packet = new PacketManager();

        //Data_Player.GetClientData().m_Current_LobbyID
        // 어떤 로비에 들어왔음을 서버에게 알려준다.(로비 ID)
        net.S_LobbyInit();

        Refresh_PlayerList();

        if (thread == null)
        {
            thread = new Thread(RunThread);
            thread.Start();
        }
    }

    public override void Update_Input()
    {
        base.Update_Input();
        switch (m_Cursor)
        {
            case 0: // 채팅
                m_Chat.Select();
                break;
            default:
                EventSystem.current.SetSelectedGameObject(null);
                break;
        }

        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            TM.Play_OKSound();
            switch (m_Cursor)
            {
                case 0: // 채팅
                    string message = m_Chat.text.Replace("\n", "");

                    if (message.Equals("")) // 아무 입력도 안했으면 무시
                        return;

                    // TODO 서버에게 자신의 채팅 문자열 보내기
                    net.S_Chat(message);

                    // 채팅창이 바로 바뀌지는 않고, 서버에게서 패킷을 받았을 때 수정됨

                    m_Chat.text = ""; // 일봤으면 텍스트는 지워야지
                    m_Chat.Select(); // GUI 포커스 재설정

                    // 임시 - 채팅 윈도우 테스트
                    //Add_ChatMessage(message);
                    // 임시 끝
                    break;
                case 1: // 준비
                    Data_Player.GetClientData().m_Lobby_GetReady = !Data_Player.GetClientData().m_Lobby_GetReady;

                    // TODO 서버에게 나 준비했어요 보내기
                    net.S_Ready(Data_Player.GetClientData().m_Lobby_GetReady);
                    // 이것도 패킷을 받아야 수정됨

                    Refresh_PlayerList();
                    break;
                case 2: // 어 이 방이 아니네
                    // TODO 서버에게 나 나가요 보내기
                    net.ExitLobby();

                    thread.Abort();
                    thread = null;
                    // 플레이어가 현재 접속한 로비의 ID를 초기화
                    Data_Player.GetClientData().m_Current_LobbyID = -1;
                    Data_Player.GetClientData().m_Lobby_GetReady = false;

                    Data_Lobby.Current_Lobby = null;

                    TM.m_State = Title_Manager.MENU_STATE.LOBBYLIST;
                    TM.Refresh_Window();
                    // 빠르게 입력하면 버그가 있으니까 그걸 방지하기 위해서 인보크나 코루틴을 사용해 로딩 텀을 주는게 좋을거같다.
                    break;
            }
        }
    }

    #region Thread

    private Thread thread = null;

    private object lockObject = new object();       // 임계영역 처리를 위한 오브젝트 생성
    private static Queue<byte[]> TaskQueue = new Queue<byte[]>();

    private class Task
    {
        public byte[] buffer = new byte[1024];
    }

    void Update()
    {
        while (TaskQueue.Count > 0)
        {
            Task task = new Task();

            task.buffer = TaskQueue.Dequeue();

            UInt64 protocol = 0;

            packet.GetProtocol(task.buffer, ref protocol);

            int netID;
            string nickName;
            bool isReady;
            string msg;

            net.LobbyState_UnPackProtocol(ref protocol);

            switch (protocol)
            {
                case (UInt64)LobbyManager_Protocol.LOBBY_INIT:
                    // [Server] 방에 들어가면 자기 자신의 NetID를 할당한다.
                    net.R_LobbyInit(task.buffer, ref Data_Player.GetClientData().m_PlayerNetID);

                    //Debug.Log("내번호 = " + Data_Player.GetClientData().m_PlayerNetID);

                    break;

                case (UInt64)LobbyManager_Protocol.PLAYERLIST:
                    netID = 0;
                    nickName = "";
                    isReady = false;

                    net.SetPlayerList(task.buffer, ref netID, ref nickName, ref isReady);

                    Getted_PlayerData(netID, nickName, isReady);
                    break;

                case (UInt64)LobbyManager_Protocol.READY:
                    netID = 0;
                    isReady = false;

                    net.R_Ready(task.buffer, ref netID, ref isReady);

                    Getted_PlayerReady(netID, isReady);

                    break;

                case (UInt64)LobbyManager_Protocol.CHATTING:
                    msg = "";
                    net.R_Chat(task.buffer, ref msg);

                    Getted_Chat(msg);
                    break;

                    //[Server] 다른 유저가 나갔을 때 받을 패킷
                case (UInt64)LobbyManager_Protocol.EXIT:
                    int Exit_Player_ID = 0;
                    net.ExitPlayer(task.buffer, ref Exit_Player_ID);

                    Getted_PlayerLeave(Exit_Player_ID);
                    break;

                case (UInt64)LobbyManager_Protocol.LOBBY_GAME_START:
                    thread.Abort();
                    thread = null;

                    Getted_GameStart();
                    break;
            }

        }
    }

    // 쓰레드 함수 구현
    void RunThread()
    {
        TaskQueue.Clear();

        while (true)
        {
            Task task = new Task();

            net.PacketRecv(ref task.buffer);

            // 처리
            TaskQueue.Enqueue(task.buffer);
        }
    }

    #endregion



    #region PACKET

    // TODO Getted_PlayerData
    /// <summary>
    /// 플레이어 데이터를 받음
    /// 원하는 타이밍은 처음 로비에 접속했을 때
    /// </summary>
    public void Getted_PlayerData(int _ID, string _name, bool _ready)
    {
        // 로비의 플레이어들을 대상
        foreach(Data_Player player in m_Login_Players)
        {
            // 패킷으로 받은 ID와 플레이어의 NetID가 일치하는 경우 해당 데이터를 수정
            if (player.m_PlayerNetID == _ID)
            {
                player.m_Name = _name;
                player.m_Lobby_GetReady = _ready;
                Refresh_PlayerList();
                return;
            }
        }
        // 로비의 플레이어들 목록에 데이터가 없으면, 새로운 플레이어 데이터를 작성해서 리스트에 처넣음
        Data_Player temp_player = new Data_Player
        {
            m_PlayerNetID = _ID,
            m_Name = _name,
            m_Lobby_GetReady = _ready
        };
        m_Login_Players.Add(temp_player);
        Refresh_PlayerList();
    }

    // TODO Getted_PlayerJoin
    /// <summary>
    /// 누군가가 로비에 들어옴
    /// 레디는 당연히 안했으니 ID와 이름만 받아도 될듯
    /// </summary>
    /// <param name="_ID">유저 NetID</param>
    /// <param name="_name">유저 이름</param>
    public void Getted_PlayerJoin(int _ID, string _name)
    {
        // 새로운 데이터를 작성
        Data_Player new_user = new Data_Player
        {
            m_PlayerNetID = _ID,
            m_Name = _name,
            m_Lobby_GetReady = false
        };
        m_Login_Players.Add(new_user);
        Refresh_PlayerList();
    }

    // TODO Getted_PlayerLeave
    /// <summary>
    /// 누군가가 로비를 나감
    /// </summary>
    /// <param name="_ID">유저 NetID</param>
    public void Getted_PlayerLeave(int _ID)
    {
        // 리스트에서 해당 플레이어를 빼기
        foreach (Data_Player player in m_Login_Players)
        {
            if (player.m_PlayerNetID == _ID)
            {
                m_Login_Players.Remove(player);
                break;
            }
        }

        Refresh_PlayerList();
    }

    // TODO Getted_PlayerReady
    /// <summary>
    /// 누군가가 레디함
    /// _ID를 기반으로 플레이어들의 ID와 비교해, 같은 플레이어의 m_Lobby_GetReady를 _Ready로 변경
    /// </summary>
    /// <param name="_ID">유저 NetID</param>
    /// <param name="_Ready">유저 Ready 상태</param>
    public void Getted_PlayerReady(int _ID, bool _Ready)
    {
        foreach (Data_Player player in m_Login_Players)
        {
            if (player.m_PlayerNetID == _ID)
            {
                player.m_Lobby_GetReady = _Ready;
                break;
            }
        }
        Refresh_PlayerList();
    }

    // TODO Getted_GameStart
    /// <summary>
    /// 게임이 시작됨
    /// </summary>
    public void Getted_GameStart()
    {
        Data_Lobby.Current_Lobby.m_Current_User = m_Login_Players.Count;
        Data_Lobby.Current_Lobby.m_Now_Playing = true;
        Data_Lobby.Current_Lobby.m_Players = m_Login_Players;

        Scene_Loader sl = Instantiate(Scene_Loader_Prefab).GetComponent<Scene_Loader>();
        sl.To_Ingame();
    }

    // TODO Getted_Chat
    /// <summary>
    /// 서버 채팅 메시지를 받음
    /// 채팅 메시지는 구성이 완벽해야함
    /// 누구누구: 대사대사 아 뭐 원하는대로 하새우
    /// </summary>
    /// <param name="_text">텍스트 내용</param>
    public void Getted_Chat(string _text)
    {
        Add_ChatMessage(_text);
    }
    
    #endregion

    #region LOBBY METHOD

    /// <summary>
    /// 플레이어 리스트 텍스트 갱신
    /// 거의 대부분의 패킷을 받을 때 발생한다
    /// </summary>
    public void Refresh_PlayerList()
    {
        string desc = "[ PLAYER LIST ]";
        foreach (Data_Player player in m_Login_Players)
            desc += "\n" + (player.m_Lobby_GetReady ? "READY - " : "") + player.m_Name;
        m_PlayerList.text = desc;
    }

    /// <summary>
    /// 채팅창에 메시지를 추가
    /// 채팅창에는 아쉽게도 스크롤 기능이 없음
    /// </summary>
    public void Add_ChatMessage(string _text)
    {
        Element_Chat chat;
        if (m_Chats.Count > 15)
        {
            chat = m_Chats[m_Chats.Count - 1];
            m_Chats.Remove(chat);
        }
        else
            chat = Instantiate(Chat_Prefab, m_Chat_Mask).GetComponent<Element_Chat>();
        chat.Init(_text);
        m_Chats.Insert(0, chat);

        for(int i = 0; i < m_Chats.Count; i++)
        {
            RectTransform rt = m_Chats[i].GetComponent<RectTransform>();
            rt.anchoredPosition = new Vector2(0f, 45f * i);
        }
    }

    #endregion
}
