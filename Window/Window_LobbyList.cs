﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Malaia.Data;

public class Window_LobbyList : Window_Base
{
    public NetworkManager net;
    public Text Users;                                    // 현재 유저 몇 명 있나 알려주는 애
    public Text ErrorText;                              // 오류 텍스트 띄워주는 애
    public RectTransform LobbyList_Axis;        // 로비 리스트의 스크롤 축
    public GameObject Lobby_Prefab;             // 로비 엘리먼트 프리팹
    List<Element_Lobby> m_Lobby_Objects;     // 만들어진 로비 엘리먼트 리스트

    int m_List_Cursor;                                    // 로비 리스트 커서 값
    bool m_Is_Focus_List;                               // 로비 리스트로 포커스가 옮겨갔는지 여부
    Element_Lobby m_Current_Focus_Lobby;   // 현재 포커스된 로비 오브젝트

    public Window_LobbyList()
    {
        m_Menus = new string[3] { "CREATE", "REFRESH", "EXIT" };
    }

    // Title_Manager가 이 윈도우를 부르면서 Initialize 한다

    public void SetLobby() // 로비 세팅     
    {
        // [Server] 서버에게로 로비 리스트 데이터 요청
        net.ShowLobby();

        // [Server] 로비 개수가 총 몇개있는지 먼저 받는다.
        int roomCount = net.LobbyCount();

        for (int i = 0; i < roomCount; i++)
        {
            string id = null;
            int roomNumber = 0;
            int playerCount = 0;
            bool isPlay = false;

            // [Server] 로비 개수만큼 데이터를 받아 각각의 로비 데이터에 추가한다.
            net.LobbyInfo(ref roomNumber, ref id, ref playerCount, ref isPlay);
            Add_Lobby(new Data_Lobby { m_ID = roomNumber, m_Lobby_Name = id, m_Current_User = playerCount, m_Now_Playing = isPlay });
        }
    }

    public override void Initialize()
    {
        // 변수 초기화
        m_List_Cursor = 0;
        m_Is_Focus_List = false;
        Users.text = "";
        ErrorText.text = "";
        Refresh_LobbyList();

        // TODO 서버한테 현재 서버에 몇 명이 있나 알려달라고 하기
        // TODO 서버한테 현재 어떤 로비 있나 알려달라고 하기
        // [Server] 서버에게서 로비 리스트를 받는다.
        SetLobby();
    }

    public override void Update_Input()
    {
        if(m_Is_Focus_List) // 현재 포커스가 로비 리스트를 가리키는 경우
        {
            if (Input.GetKeyDown(KeyCode.DownArrow)) // 리스트 커서 변경
            {
                TM.Play_ClickSound();
                m_List_Cursor = m_List_Cursor + 1 > m_Lobby_Objects.Count - 1 ? 0 : m_List_Cursor + 1;
                Change_Focus(m_Lobby_Objects[m_List_Cursor]);
                Change_List_Axis();
            }
            if (Input.GetKeyDown(KeyCode.UpArrow)) // 리스트 커서 변경
            {
                TM.Play_ClickSound();
                m_List_Cursor = m_List_Cursor - 1 < 0 ? m_Lobby_Objects.Count - 1 : m_List_Cursor - 1;
                Change_Focus(m_Lobby_Objects[m_List_Cursor]);
                Change_List_Axis();
            }
            if (Input.GetKeyDown(KeyCode.RightArrow)) // 리스트 <-> 메뉴 오가기
            {
                TM.Play_ClickSound();
                m_Is_Focus_List = false;
                Change_Focus(null);
            }
            if (Input.GetKeyDown(KeyCode.Return)) // 현재 선택한 로비로 접속 시도
            {
                TM.Play_OKSound();

                // TODO 서버에게 m_Current_Focus_Lobby.m_Data.m_ID에 해당하는 로비로 접속 시도하기
                net.EnterLobby(m_Current_Focus_Lobby.m_Data.m_ID, m_Current_Focus_Lobby.m_Data.m_Lobby_Name);

                if(net.EnterLobbyResult())
                {
                    //여기서 성공하면
                    Getted_LobbyJoin_Success(m_Current_Focus_Lobby.m_Data.m_ID);
                }
                else
                {
                    Getted_LobbyJoin_Failed("방 접속 실패!");
                }
            }
        }
        else // 포커스가 메뉴를 가리키는 경우
        {
            base.Update_Input();
            if(Input.GetKeyDown(KeyCode.LeftArrow)) // 리스트 <-> 메뉴 오가기
            {
                TM.Play_ClickSound();
                if(m_Lobby_Objects.Count > 0) // 로비 오브젝트 리스트에 뭐가 하나라도 있어야지만 리스트로 커서 변경이 가능
                {
                    m_Is_Focus_List = true;
                    Change_Focus(m_Lobby_Objects[m_List_Cursor]);
                }
            }

            if (Input.GetKeyDown(KeyCode.Return))
            {
                TM.Play_OKSound();
                switch (m_Cursor)
                {
                    case 0: // 방 만들기
                        // TODO 서버에게 방 만든다고 신호보내기
                        int m_ID = net.CreateLobby();
                        Data_Player.GetClientData().m_PlayerNetID = 1;
                        Getted_LobbyJoin_Success(m_ID);
                        break;
                    case 1: // 리스트 갱신
                        Refresh_LobbyList(); // 로비 리스트 비우기 (모든 로비 데이터를 새로 받을거니까)

                        // TODO 서버한테 현재 서버에 몇 명이 있나 알려달라고 하기
                        // TODO 서버한테 현재 어떤 로비 있나 알려달라고 하기
                        SetLobby();
                        // 이거 위의 Initialize 하면서 서버에게 묻는거랑 똑같음
                        break;
                    case 2: // 서버 나가기
                        // TODO 서버에게 나 나가요 보내기
                        net.LogOut();                        

                        TM.m_State = Title_Manager.MENU_STATE.MAIN;
                        TM.Refresh_Window();
                        break;
                }
            }
        }
    }
    
    /// <summary>
    /// 로비 리스트 초기화
    /// </summary>
    public void Refresh_LobbyList()
    {
        m_List_Cursor = 0; // 리스트 커서를 0으로
        // 리스트가 만들어졌는데 뭐가 하나 이상 있는 경우
        if (m_Lobby_Objects != null && m_Lobby_Objects.Count > 0)
        {
            foreach (Element_Lobby lobby in m_Lobby_Objects)
                Destroy(lobby.gameObject);
        }
        m_Lobby_Objects = new List<Element_Lobby>();
    }

    /// <summary>
    /// 로비 리스트에 로비 추가
    /// </summary>
    public void Add_Lobby(Data_Lobby _data)
    {
        Element_Lobby lobby = Instantiate(Lobby_Prefab, LobbyList_Axis).GetComponent<Element_Lobby>();
        lobby.Init(_data);
        lobby.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -150f * m_Lobby_Objects.Count);
        m_Lobby_Objects.Add(lobby);
    }

    /// <summary>
    /// 로비 오브젝트 선택을 변경
    /// </summary>
    /// <param name="_lobby"></param>
    public void Change_Focus(Element_Lobby _lobby)
    {
        if(m_Current_Focus_Lobby != null)
            m_Current_Focus_Lobby.Toggle_Focus();
        m_Current_Focus_Lobby = _lobby;
        if (m_Current_Focus_Lobby != null)
            m_Current_Focus_Lobby.Toggle_Focus();
    }

    /// <summary>
    /// 로비 리스트 커서가 바꼈을 때 리스트의 축을 변경
    /// </summary>
    public void Change_List_Axis()
    {
        if (m_Lobby_Objects.Count > 3 && m_List_Cursor > 3)
        {
            LobbyList_Axis.anchoredPosition = new Vector2(0f, 150f * (m_List_Cursor - 3));
        }
        else
        {
            LobbyList_Axis.anchoredPosition = Vector2.zero;
        }
    }

    #region PACKET

    // TODO Getted_LobbyData
    /// <summary>
    /// 서버로부터 로비 데이터 취득
    /// </summary>
    /// <param name="_lobbyID">로비 ID</param>
    /// <param name="_current_user_count">현재 유저 수</param>
    /// <param name="_is_playing">게임이 플레이중인가</param>
    /// <param name="_lobby_name">로비의 이름</param>
    public void Getted_LobbyData(int _lobbyID, int _current_user_count, bool _is_playing, string _lobby_name)
    {
        // 새로운 Data_Lobby 변수를 작성
        Data_Lobby data = new Data_Lobby
        {
            m_ID = _lobbyID,
            m_Current_User = _current_user_count,
            m_Now_Playing = _is_playing,
            m_Lobby_Name = _lobby_name
        };

        Add_Lobby(data); // 로비 오브젝트를 로비 리스트에 추가
    }

    // TODO Getted_ServerPlayerCount
    /// <summary>
    /// 서버로부터 유저 수를 취득
    /// </summary>
    /// <param name="_count">서버 접속자 수</param>
    public void Getted_ServerPlayerCount(int _count)
    {
        Users.text = _count + " USERS ";
    }

    // TODO Getted_LobbyJoin_Success
    /// <summary>
    /// 서버로부터 방 접속 허락이 떨어짐
    /// 방을 개설하기로 한 경우에도 어차피 별로 변화가 있지 않으니, 로비 접속은 그냥 이걸로 퉁치면 될듯?
    /// </summary>
    public void Getted_LobbyJoin_Success(int _lobbyID)
    {
        // 플레이어가 현재 접속한 로비의 ID를 설정
        Data_Player.GetClientData().m_Current_LobbyID = _lobbyID;

        if (m_Current_Focus_Lobby != null)
            Data_Lobby.Current_Lobby = m_Current_Focus_Lobby.m_Data;
        else
            Data_Lobby.Current_Lobby = new Data_Lobby()
            {
                m_ID = _lobbyID,
                m_Current_User = 1,
                m_LobbyLeaderID = Data_Player.GetClientData().m_PlayerNetID
            };

        // 개설한 로비로 이동
        TM.m_State = Title_Manager.MENU_STATE.LOBBY;
        TM.Refresh_Window();
    }

    // TODO Getted_LobbyJoin_Failed
    /// <summary>
    /// 서버로부터 방 접속/개설이 차단됨
    /// 뭐 이런저런 이유로
    /// </summary>
    public void Getted_LobbyJoin_Failed(string _error_text)
    {
        ErrorText.text = "ERROR: " + _error_text;
    }

    #endregion
}