﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Element_Chat : MonoBehaviour
{
    public Text ChatText;

    public void Init(string _text)
    {
        RectTransform rt = GetComponent<RectTransform>();
        rt.localPosition = Vector3.zero;
        rt.localScale = new Vector3(1f, 1f, 1f);

        ChatText.text = _text;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
